#!/usr/bin/python3
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import requests, time, sys, os, sqlite3, hashlib, platform, subprocess#, re

BLUE, RED, YELLOW, GREEN, END = '\33[94m', '\033[91m', '\33[93m', '\033[1;32m', '\033[0m'

#urllib3.disable_warnings()
os.system('clear')
if platform.system() is "Windows":
    os.system("cls")

if platform.system() is "Linux":
    os.system('clear')

def hashMD5(my_string):
    m = hashlib.md5()
    m.update(my_string.encode('utf-8'))
    return m.hexdigest()

if os.path.isfile('./data.db.enc') is True:
    if os.path.isfile('./data.db') is True:

        if platform.system() is "Windows":
            os.system("del data.db")
        else:
            os.system('rm data.db')

    print("Tippe dein Passwort ein:")
    z_password = input("$» ")

    if platform.system() is "Windows":
       os.system(r'"C:\Program Files\7-Zip\7z.exe" x data.enc.zip -p' + hashMD5(z_password))

       if os.path.isfile('./data.db') is True:
          print("Das Passwort ist falsch!")
          os.system("del data.db")
          exit()
    else:
          #os.system('unzip -P ' + hashMD5(z_password) + ' data.enc.zip')
          os.system('7za e -p' + hashMD5(z_password) +' data.db.enc')

elif os.path.isfile('./data.db') is True:
    print("Achtung! Wir empfehlen dir deine Daten zu verschlüsseln, starte einfach das setup.py Skript neu und gebe an das du verschlüsseln möchtest.")
else:
    print("ERROR: database does not exist.")
    a=2
    while True:
        print("Möchtest du die Setup Datei starten? [Y/n]")
        wantToEnc = input("[y/N]» ")

        if wantToEnc.lower().strip() == "y":
            #os.system('python setup.py')
            try:
                subprocess.call(['python3 ' + 'setup.py'])
            except OSError:
                print ('Versuche mit Python3 zu starten [python3 setup.py] || Failed')
                try:
                    subprocess.call('python setup.py')
                except OSError:
                    print ('Versuche mit Python zu starten [python setup.py] || Failed\nVersuche es noch ein mal! Sollte dieser Fehler noch einmal auftreten, \nklicke auf diesen Link: https://gitlab.com/mindcrew-dev/httf-register/issues und schreibe uns deinen Fehler.')
                    exit()
        elif wantToEnc.lower().strip() == "n":
            print("Du solltest das setup script von Hand starten.")
            exit()
        else:
                print("ERROR: \n" + "'" + wantToEnc + "'" + " ist keine Auswahlmöglichkeit.  \n Versuch es nochmal!")


if os.path.isfile('./geckodriver.log') is True:
    os.system('rm geckodriver.log')
    if platform.system() is "Windows":
        os.system("del geckodriver.log")



def dl(bvs = "", afs = ""): #dividing line
    print(bvs + "--------------------------------------------------------------------------------" + afs)



def heading():
        spaces = " " * 76
        sys.stdout.write(GREEN + spaces + """
         __      __ ____________ ____________ ________
        |  |    |  |_____   ____|_____   ____|   _____|
        |  |____|  |    |  |         |  |    |  |_____
        |   ____   |    |  |         |  |    |  ______|
        |  |    |  |    |  |         |  |    |  |
        |__|    |__|    |__|         |__|    |__|
                ____     _________   _________
               |    |   |   ______| |   ______|
              |  __  |  |  |        |  |
              | |__| |  |  |        |  |
             |   __   | |  |______  |  |______
             |__|  |__| |_________| |_________|

 ______ _____ _     _ _____ ______   __ ______ _____ ______
|  ____|  ___| \   | |  ___|  __  | |  |___  _|     |  __  |
| | ___| |___|   \ | | |___| |__| || __ | | | |  _  | |__| |
| ||_  |  ___|  \    |  ___|  __ | ||__|| | | | |_| |  __ |
| |__| | |___| | \   | |___| |  \ \| __  || | |     | |  \ \\
|______|_____|_|  \|_|_____|_|   \|_|  |_||_| |_____|_|   \_\\

        """ + END + BLUE +
        #'\n' + '{}Kick Devices Off Your LAN ({}KickThemOut{}){}'.format(YELLOW, RED, YELLOW, BLUE).center(98) +
        '\n' + 'Made by: {0}Sascha S. ({1}Mv0sKff{2}) & {0}Maris B. ({1}TheProgramming_M{2}){3}'.format(YELLOW, RED, YELLOW, BLUE).center(111) +
        '\n' + 'Version: {}2.1{} \n'.format(YELLOW, END).center(86))
        print("\nWenn du Probleme mit diesem Script hast, poste sie hier: https://gitlab.com/shyftr/httf-register/-/issues\n")
        time.sleep(1.5)


heading()



def cex(var_name, db_name, ERRORst = "ERROR: variable ERRORst is not defined"):
    c.execute("SELECT " + db_name + " FROM data")
    var_name, = c.fetchone()
    if var_name is "" and ERRORst != "PREDF":# and var_name != "_db_laptop":
        print(ERRORst)
        exit


#connect do database
conn = sqlite3.connect("data.db")
c = conn.cursor()

c.execute("SELECT _forename FROM data")
forenamevar, = c.fetchone()
if forenamevar is "":
    print("Du musst deinen Vornamen angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _lastname FROM data")
namevar, = c.fetchone()
if namevar is "":
    print("Du musst deinen Nachnamen angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _street FROM data")
streetvar, = c.fetchone()
if streetvar is "":
    print("Du musst deine Straße angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _house_number FROM data")
house_numbervar, = c.fetchone()
if house_numbervar is "":
    print("Du musst deine Hausnummer angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _plz FROM data")
plzvar, = c.fetchone()
if plzvar is "":
    print("Du musst deine Postleitzahl angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _city FROM data")
cityvar, = c.fetchone()
if cityvar is "":
    print("Du musst deine Stadt angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _email FROM data")
emailvar, = c.fetchone()
if emailvar is "":
    print("Du musst deine E-Mail angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _gender FROM data")
gendervar, = c.fetchone()

c.execute("SELECT _birthday FROM data")
birthdayvar, = c.fetchone()
if birthdayvar is "":
    print("Du musst dein Geburtsdatum angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _phone_number FROM data")
phone_numbervar, = c.fetchone()
if phone_numbervar is "":
    print("Du musst deine Telefon/Handynummer angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _allergic FROM data")
allergicvar, = c.fetchone()

c.execute("SELECT _parent_name FROM data")
parentname_var, = c.fetchone()
if parentname_var is "":
    print("Du musst den Vor- und Nachnamen eines Erziehungsberechtigten angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _phone_number_parent FROM data")
phone_number_parentvar, = c.fetchone()
if phone_number_parentvar is "":
    print("Du musst die Telefonnummer eines Erziehungsberechtigten angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _email_parent FROM data")
email_parentvar, = c.fetchone()
if email_parentvar is "":
    print("Du musst die E-Mail eines Erziehungsberechtigten angeben! Bitte starte die setup Datei neu!")
    exit()

c.execute("SELECT _db_have_programmed FROM data")
db_have_programmed, = c.fetchone()

c.execute("SELECT _programming_language FROM data")
languagevar, = c.fetchone()

c.execute("SELECT _db_hardware FROM data")
db_hardware, = c.fetchone()

c.execute("SELECT _which_skills FROM data")
which_skillsvar, = c.fetchone()

c.execute("SELECT _what_try FROM data")
what_tryvar, = c.fetchone()

c.execute("SELECT _keen_on FROM data")
keenonvar, = c.fetchone()

c.execute("SELECT _cando FROM data")
candovar, = c.fetchone()

c.execute("SELECT _db_laptop FROM data")
db_laptop, = c.fetchone()
if db_laptop == "undefined":
    print("Du musst angeben, ob du einen Laptop zu HTTF mitbringst! Bitte starte die setup Datei neu!")
    exit()


print("Füge hier den Link der HTTF Anmeldeseite ein: ")
url = input("URL: ")

print("Möchtest du deine Angaben nach dem sie im Formular angegeben wurden überprüfen?\nWenn du sie nach dem ausfüllen überprüfen willst schreibe 'y', drücke ENTER!\nWenn sie sofort ohne überprüfung abgeschickt werden sollen, drücke ENTER!")
correct = input("Antwort: ")
dl()
print("Bitte gebe 'Hostel' ein und drücke ENTER, wenn du im Hostel übernachten willst!\nWenn du zu Hause schläfst, drücke ENTER!")
hostel_q = input("Antwort: ")
dl()
print("Bitte gebe 'x' ein und drücke ENTER, wenn du schon einmal bei einem HTTF warst!\nWenn dies das erste mal bei einem HTTF ist, drücke ENTER!")
stillhere_q = input("Antwort: ")


def sef(var, id, keys):
    var = browser.find_element_by_id(id)
    var.send_keys(keys)

def csb(var, id):
    var = browser.find_element_by_id(id)
    var.click()


browser = webdriver.Firefox(executable_path='./geckodriver')
browser.get((url))
time.sleep(4)

sef("forename", 'powermail_field_vorname', forenamevar)
sef("name", 'powermail_field_name', namevar)
sef("street", 'powermail_field_strae', streetvar)
sef("house_number", 'powermail_field_hausnummer', house_numbervar)
sef("plz", 'powermail_field_postleitzahl', plzvar)
sef("city", 'powermail_field_stadt', cityvar)
sef("email", 'powermail_field_e_mail_adresse', emailvar)

try:
    if 'w' in gendervar or 'W' in gendervar:
        csb("female", 'powermail_field_geschlecht_1')
    elif 'm' in gendervar or 'M' in gendervar:
        csb("male", 'powermail_field_geschlecht_2')
    elif 'd' in gendervar or 'D' in gendervar:
        csb("undefined", 'powermail_field_geschlecht_3')
    else:
        print("Dein Geschlecht konnte nicht eingestellt werden, bitte starte die setup Datei neu!")
        exit()
except:
    print("Bitte trage dein Geschlecht selber in das Formular ein.")

sef("birthday", 'powermail_field_marker', birthdayvar)
try:
    sef("phone_number", 'powermail_field_telefonnummer', phone_numbervar)
except:
    pass
try:
    sef("allergic", 'powermail_field_nahrungsmittelunvertrglichkeiten_besonderheiten', allergicvar)
except:
    pass
try:
    sef("parent_name", 'powermail_field_namevornameelternteilerziehungsberechtiger', parentname_var)
except:
    pass
try:
    sef("phone_number_parent", 'powermail_field_telefonnummerdeinerelternerziehungsberichtigten', phone_number_parentvar)
except:
    pass
try:
    sef("email_parent", 'powermail_field_e_mail_adresseelternteil', email_parentvar)
except:
    pass

if "yes" in db_have_programmed:
    csb("have_programmed_y", 'powermail_field_hastduschonmaletwasprogrammiert_1')
else:
    csb("have_programmed_y", 'powermail_field_hastduschonmaletwasprogrammiert_2')

sef("language", 'powermail_field_mitwelchenprogrammiersprachenhastduschongearbeitet', languagevar)
try:
    sef("what_programmed", 'powermail_field_washastduschonprogrammiert', which_skillsvar)
except:
    pass
if "1" in db_hardware:
    csb("arduino", 'powermail_field_marker_01_1')

if "2" in db_hardware:
    csb("rpi", 'powermail_field_marker_01_2')

if "3" in db_hardware:
    csb("call", 'powermail_field_marker_01_3')

if "4" in db_hardware:
    csb("bob3", 'powermail_field_marker_01_4')

if "" in db_hardware and "1" not in db_hardware and "2" not in db_hardware and "3" not in db_hardware and "4" not in db_hardware:
    csb("nothing", 'powermail_field_marker_01_5')

try:
    sef("cando", 'powermail_field_welcheweiterenfhigkeitenknntestdubeimhackathoneinbringen', candovar)
except:
    pass
try:
    sef("what_try", 'powermail_field_waswrdestdubeihacktothefuturegernmalausprobieren', what_tryvar)
except:
    pass
try:
    sef("keen_on", 'powermail_field_warummchtestdubeihacktothefuturemitmachen', keenonvar)
except:
    pass
try:
    if "need" in db_laptop:
        sef("need_lap", 'powermail_field_mchtestdufrdenhackathoneinenlaptop_2',"")
    else:
        csb("got_one", 'powermail_field_mchtestdufrdenhackathoneinenlaptop_1')
except:
    pass
try:
    if "Hostel" in hostel_q or "hostel" in hostel_q:
        csb("jump_in", 'powermail_field_marker_05_1')
    else:
        csb("bedroom", 'powermail_field_marker_05_2')

    if "x" in stillhere_q or "X" in stillhere_q:
        csb("i_was", 'powermail_field_marker_04_1')
    else:
        csb("first_time", 'powermail_field_marker_04_2')
except:
    pass

csb("agb", 'powermail_field_bdatenschutzb_1')

if "y" in correct or "Y" in correct:
    print("Bitte überprüfe deine Angaben im Browser!")
    exit()
else:
    agb = browser.find_element_by_id('powermail_field_bdatenschutzb_1')
    agb.submit()

if os.path.isfile('./data.enc.zip') is True:
    os.system('rm data.db')
    if platform.system() is "Windows":
        os.system("del data.db")
