/*!
 File input replacement,
 copyright: Matthias Kappenberg, www.the-dimension.com
 */


(function($) {
    $.fn.replaceFileInput = function(options) {
        var settings = {
            width : '100%',
            height : '100%'
        };
        if(options) {
            $.extend(settings, options);
        }
        return this.each(function() {

            var $self = $(this),
                wrapper = $('<span class="fileinput-replace"></span>'),
                wrapperFile = $('<span class="fileinput-wrapper"></span>'),
                filename = $('<span class="fileinput-replace-text"></span>'),
                browse = $('<span class="fileinput-replace-browse"></span>');

            filename.text('Datei auswählen');
            browse.text('Durchsuchen');

            $self
                .wrap(wrapper)
                .before(browse)
                .before(filename)
                .wrap(wrapperFile);

            $self.css({
                "position": "relative",
                "width": settings.width,
                "height": settings.height,
                "display": "inline",
                "cursor": "pointer",
                "opacity": "0.0",
                "margin-left": settings.width
            });

            $self.bind("change", function() {
                filename.text($self.val());
            });

        });
    };
})(jQuery);

$(document).ready(function () {
    $(".image-caption").each(function(i) {var textInside =  this.innerHTML; this.innerHTML = textInside.trim()})
    $('.powermail_fieldwrap_file input[type=file]').replaceFileInput({
        width : '100%',
        height: '30px'
    });
});