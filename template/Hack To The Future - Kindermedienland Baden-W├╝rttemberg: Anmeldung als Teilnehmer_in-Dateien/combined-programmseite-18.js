/* combined 50 files:
 * - modules/mod-answerform/js/mod-answerform.js
 * - modules/mod-article/js/mod-article.js
 * - modules/mod-articleFooterLinks/js/mod-articleFooterLinks.js
 * - modules/mod-articlelist/js/mod-articlelist.js
 * - modules/mod-articleteaser/js/mod-articleteaser.js
 * - modules/mod-associationListTeaser/js/mod-associationListTeaser.js
 * - modules/mod-associationTeaser/js/mod-associationTeaser.js
 * - modules/mod-banner/js/mod-banner.js
 * - modules/mod-calculateTeaser/js/mod-calculateTeaser.js
 * - modules/mod-commentform/js/mod-commentform.js
 * - modules/mod-contactform/js/mod-contactform.js
 * - modules/mod-dev-toolbar/js/mod-dev-toolbar.js
 * - modules/mod-direktkontakt/js/mod-direktkontakt.js
 * - modules/mod-dossier-header/js/mod-dossier-header.js
 * - modules/mod-eventCalender/js/mod-eventCalender.js
 * - modules/mod-eventCalenderItem/js/mod-eventCalenderItem.js
 * - modules/mod-eventform/js/mod-eventform.js
 * - modules/mod-fakebookBox/js/mod-fakebookBox.js
 * - modules/mod-footer/js/mod-footer.js
 * - modules/mod-footerSekundaer/js/mod-footerSekundaer.js
 * - modules/mod-gallery/js/mod-gallery.js
 * - modules/mod-global-search/js/mod-global-search.js
 * - modules/mod-glossary/js/mod-glossary.js
 * - modules/mod-imageGallery/js/mod-imageGallery.js
 * - modules/mod-loginAreaItem/js/mod-loginAreaItem.js
 * - modules/mod-loginAreaList/js/mod-loginAreaList.js
 * - modules/mod-logoandclaim/js/mod-logoandclaim.js
 * - modules/mod-main-menu/js/mod-main-menu.js
 * - modules/mod-mediathek/js/mod-mediathek.js
 * - modules/mod-multibox/js/mod-multibox.js
 * - modules/mod-newsletterform/js/mod-newsletterform.js
 * - modules/mod-pagination/js/mod-pagination.js
 * - modules/mod-partner/js/mod-partner.js
 * - modules/mod-podcastteaser/js/mod-podcastteaser.js
 * - modules/mod-portaleteaser/js/mod-portaleteaser.js
 * - modules/mod-presslist/js/mod-presslist.js
 * - modules/mod-program-logoandclaim/js/mod-program-logoandclaim.js
 * - modules/mod-program-main-menu/js/mod-program-main-menu.js
 * - modules/mod-publicationsCenter/js/mod-publicationsCenter.js
 * - modules/mod-search/js/mod-search.js
 * - modules/mod-service-menu/js/mod-service-menu.js
 * - modules/mod-skipnavi/js/mod-skipnavi.js
 * - modules/mod-slider/js/mod-slider.js
 * - modules/mod-slider-dossier/js/mod-slider-dossier.js
 * - modules/mod-targetGroupTeaser/js/mod-targetGroupTeaser.js
 * - modules/mod-timeline/js/mod-timeline.js
 * - modules/mod-top-menu/js/mod-top-menu.js
 * - modules/mod-video/js/mod-video.js
 * - modules/mod-votingteaser/js/mod-votingteaser.js
 * - modules/mod-votingteaser2/js/mod-votingteaser2.js
 */

/* !@file modules/mod-answerform/js/mod-answerform.js */



/* !@file modules/mod-article/js/mod-article.js */
$(document).ready(function(){

	if(($('.mod-article').closest('div.row-fluid').find('div.span9').height() < $('.mod-article').closest('div.row-fluid').find('div.span3').height()) && $('.mod-article').find('div[data-rtr-ctype="press"]').size() > 0 && $(window).width() > 568) {

		$('.mod-article').find('div.well.article').first().height($('.mod-article').closest('div.row-fluid').find('div.span3').height() - 48);
	}

	$('.m-energiewende .optionsTeaser input').hide();

});


/* !@file modules/mod-articleFooterLinks/js/mod-articleFooterLinks.js */
$(document).ready(function() {
	$('.mod-articleteaser p').each(function(){
		if($(this).text().length <= 1) $(this).css('line-height','10px');
	});
});


/* !@file modules/mod-articlelist/js/mod-articlelist.js */
$(document).ready(function() {
	
	// add class to first news to display image
	if( !$('.mod-articlelist .teaser-element').first().hasClass('teaser-img') ){
		$('.mod-articlelist .teaser-element').first().addClass('teaser-img');
	}
	// apply grid to news elements
	$(".mod-articlelist .teaser-element:not(:first)").addClass('span6');
	
});


/* !@file modules/mod-articleteaser/js/mod-articleteaser.js */
$(document).ready(function() {
	
	/*
		make articleteaser cols same height
	*/
	$(window).load(function() {
		// count teaser elements
		var elements = $('.span6').find('.mod-articleteaser').size();
		var eqElements = elements/2 -1;	
		
		if( eqElements >= 0 ){
			// set equal height to matching row teaser
			for ( var i = 0; i <= Math.floor(eqElements); i++ ) {
				var maxHeight = -1;
				$('.span6').each(function(){
					var mod = $(this).find('.mod-articleteaser').eq(i);
					maxHeight = maxHeight > mod.height() ? maxHeight : mod.height();
				});
				$('.span6').each(function(){
					var mod = $(this).find('.mod-articleteaser').eq(i);
					mod.height(maxHeight);
				});
			}
		}
	});
	
});


/* !@file modules/mod-associationListTeaser/js/mod-associationListTeaser.js */
$(document).ready(function() {
	$('.mod-associationListTeaser').each(function(key,index) {
		$(index).find('.mod-body.tab-pane').flexslider({
			startAt: 0,
			slideshow: false,
			animation: 'slide',
			animationLoop: true,
			animationSpeed: 500,
			useCSS: false,				// # false > flickering footernav bug
			touch: true,
			itemWidth: 190,
			start: function(slider) {
				$(slider).closest('.well').find('.flex-prev').html($(slider).closest('.well').find('.tab-pane').attr('data-prev'));
				$(slider).closest('.well').find('.flex-next').html($(slider).closest('.well').find('.tab-pane').attr('data-next'));
				var associationHeadHeight = $(slider).closest('.well').find('.mod-header').outerHeight(true);
				var associationWellHeight = $(slider).closest('.well').height();
				$(slider).closest('.well').find('.mod-body').height(associationWellHeight-associationHeadHeight);
			}
		});
	});
});


/* !@file modules/mod-associationTeaser/js/mod-associationTeaser.js */
$(document).ready(function() {
	$('.mod-associationTeaser .text').focus(function(ev) {
		var tmp = $(this).val();
		if (tmp == " ...") {
			$(this).val('');
		}
	});
});


/* !@file modules/mod-banner/js/mod-banner.js */
$(document).ready(function() {

	$('.mod-banner .bannerSelect').each(function() {
		var link = $(this).find('div');
		var dropdown = $(this).find('ul');
		$(this).bind('touchend touch',function() {
			$(dropdown).show();
		});
		$(dropdown).find('li').bind('click',function(){
			$(dropdown).hide();
		});

	});
});


/* !@file modules/mod-calculateTeaser/js/mod-calculateTeaser.js */
$(document).ready(function() {
	$('.mod-calculateTeaser input.text').focus(function(ev) {
		var tmp = $(this).val();
		if (tmp == " ...") {
			$(this).val('');
		}
	});

	/*
	$('.mod-calculateTeaser input.next').on('click', function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		var text = $('.mod-calculateTeaser .mod-body input.text').val();
		$('.mod-calculateTeaser .mod-body input.text').remove();
		$('.mod-calculateTeaser .mod-body label').remove();
		$('.mod-calculateTeaser .mod-body p.unvisible').show().before('<p class="number">' + text + '</p>');
	});

*/
});


/* !@file modules/mod-commentform/js/mod-commentform.js */
(function($) {
	$.fn.extend( {
		limiter: function(limit, elem) {
			$(this).on('keyup keydown focus', function() {
				setCount(this, elem);
			});
			function setCount(src, elem) {
				var chars = src.value.length;
				if (chars > limit) {
					src.value = src.value.substr(0, limit);
					chars = limit;
				}
				if (elem && elem.length) {
					elem.html(limit - chars);
				}
			}
			setCount($(this)[0], elem);
		}
	});
})(jQuery);

$(document).ready(function() {
	if( $('.mod-comment-form textarea').length > 0 ){
		$('.mod-comment-form textarea').limiter(200);
	}
});


/* !@file modules/mod-contactform/js/mod-contactform.js */
;(function() {

	$.fn.removeFormError = function() {
		$(this).find('input, select, textarea').bind('click onkeydown', function(ev) {
			$(this).css({'background-color':'#fff'});
		});
	};

})(jQuery);


$(document).ready(function() {

	$('.mod-contactform').removeFormError();

	$('.mod-contactform input, .mod-contactform textarea').each(function() {
		if ($(this).attr('placeholder') && $(this).placeholder) $(this).placeholder();
	});
	
	
	// find required checkbox groups and change field names in order to apply correct error styles
	$('.mod-contactform .powermail_fieldwrap_check').each(function(){
		if($(this).find('.powermail_check_legend .mandatory').length){
			var inputName = $(this).find('.powermail_check_outer .powermail_check_inner:eq(0) .powermail_checkbox').attr('name');
			$(this).find('.powermail_check_outer .powermail_check_inner').each(function(){
				$(this).find('.powermail_checkbox').attr('name', inputName);
			});
		}
	});
		
	
});


 


/* !@file modules/mod-dev-toolbar/js/mod-dev-toolbar.js */
(function($) {
	$.moddevtoolbar = {};

// !$.fn.remoteAction
	$.fn.remoteAction = function (opts) {
		$(this).each(function (index, el) {
			var self = this;
			this.element = $(el);
			this.referer = this.element.parents('.dropdown').find('.dropdown-toggle');
			this.action = function() {
				if (!$.moddevtoolbar.idle) {
					$.moddevtoolbar.idle = true;

					var action = self.element.text(),
						target = self.element.attr('href');

					self.element.text('Wait for it...');
					$.getJSON(target, function(r) {
						if (r && r.status.code == 200) {
							self.referer.click();
							self.element.text(action);
							$.moddevtoolbar.idle = false;
						} else {
							self.element.text(r.status.description);
							window.setTimeout(function() {
								self.element.text(action);
								$.moddevtoolbar.idle = false;
								self.referer.click();
							}, 2000);
						}
					});
				}
			}
			this.options = jQuery.extend({
				'action': self.action
			}, opts || {});

			this.element.bind('click', function(ev) {
				ev.preventDefault();
				ev.stopPropagation();

				self.options.action(self);
			});
		});

		return this;
	}
})(jQuery);

$(document).ready(function() {
	// devbar toogler
	$('.mod-devtoolbar .toggler').click(function() {
		$(this).find('i').toggleClass('icon-minus-sign icon-plus-sign icon-white icon-black');
		$(this).next().toggle();
		$('.mod-devtoolbar').toggleClass('no-height');
	});

	$('.mod-devtoolbar .dropdown-submenu > a').bind('click', function() {
		return false;
	});

	// standard requests
	$('.mod-devtoolbar .dropdown:not(.create) a.remote').remoteAction();

	// create request with optional popup for inputs
	$('.mod-devtoolbar .dropdown.create a.remote').remoteAction({
		'action': function(ev) {
			ev.element.parents('li:last').find('a.remote').not(ev.element).popover('destroy').parent().removeClass('active');
			ev.element.popover('show').parent().addClass('active');

			var btn = $('.popover button[rel="' + ev.element.attr('rel') + '"]'),
				inp = btn.prev('input');

				inp.keyup(function() {
					if (inp.val().length && !$.moddevtoolbar.idle)
						btn.removeClass('disabled');
					else btn.addClass('disabled');
				});
				btn.mouseup(function() {
					if (!$.moddevtoolbar.idle) {
						$.moddevtoolbar.idle = true;

						var action = ev.element.text(),
							target = ev.element.attr('href') + inp.val();

						$.get(target, function(r) {
							if (r && r.status.code == 200) {
								ev.referer.click();
								ev.element.text(action);
								$.moddevtoolbar.idle = false;
							} else {
								ev.element.text(r.status.description).popover('destroy');
								window.setTimeout(function() {
									ev.element.text(action);
									$.moddevtoolbar.idle = false;
									ev.referer.click();
								}, 2000);
							}

						});
					}
				});
		}
	});

	// implement popover behavior to navbar
	$('body, .mod-devtoolbar .dropdown-toggle').click(function(ev) {
		if (!$(ev.target).is('.popover') && !$(ev.target).parents('.popover').length)
			$('.mod-devtoolbar .dropdown.open a.remote').popover('destroy').parent().removeClass('active');
		else ev.stopPropagation();
	});
});


/* !@file modules/mod-direktkontakt/js/mod-direktkontakt.js */
$(document).ready(function() {

	// add space befor @ for clear linebreak
	$('.mod-direktkontakt a, .mod-articleteaser a').each(function() {

		if ( $(this).text().search('@') > 0 && $(this).text().length > 20 ) 
		{
			$(this).html( $(this).html().replace('@',' @') );
		};
		
	});
});


/* !@file modules/mod-dossier-header/js/mod-dossier-header.js */
$(document).ready(function() {
	
	// 
	if( $('.mod-dossier-header').find('.dossier-header-element').size() > 2 && !$('.mod-dossier-header').hasClass('variant-2') ){
		$('.mod-dossier-header').addClass('variant-2');
	} else if( $('.mod-dossier-header').find('.dossier-header-element').size() <= 2 && !$('.mod-dossier-header').hasClass('variant-1') ){
		$('.mod-dossier-header').addClass('variant-1');
	}
	
});


/* !@file modules/mod-eventCalender/js/mod-eventCalender.js */



/* !@file modules/mod-eventCalenderItem/js/mod-eventCalenderItem.js */



/* !@file modules/mod-eventform/js/mod-eventform.js */
(function($) {
	$.fn.extend( {
		limiter: function(limit, elem) {
			$(this).on('keyup keydown focus', function() {
				setCount(this, elem);
			});
			function setCount(src, elem) {
				var chars = src.value.length;
				if (chars > limit) {
					src.value = src.value.substr(0, limit);
					chars = limit;
				}
				if (elem && elem.length) {
					elem.html(limit - chars);
				}
			}
			setCount($(this)[0], elem);
		}
	});

	$.fn.removeFormError = function() {
		$(this).find('input, select, textarea').bind('click onkeydown', function(ev) {
			$(this).parents('.error').removeClass('error');
		});
	};
})(jQuery);


$(document).ready(function() {

	$('.mod-eventform').removeFormError();

	$('.mod-eventform input, .mod-eventform textarea').each(function() {
		if ($(this).attr('placeholder') && $(this).placeholder) $(this).placeholder();
	});
	if( $('.mod-eventform textarea').length > 0 ){
		$('.mod-eventform textarea').limiter(200);
	}
});


 


/* !@file modules/mod-fakebookBox/js/mod-fakebookBox.js */
$(function() {
	$('.mod-fakebookBox .mod-body img').on('click', function(event) {
	 event.preventDefault();
	 /*
	 $('.mod-fakebookBox .mod-body .border').before('<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fplatform&amp;width=192&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23ffffff&amp;stream=false&amp;header=false&amp;appId=123103521159054" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:258px;" allowTransparency="true"></iframe>');
	 $('.mod-fakebookBox .mod-body img').remove();
	 */
	
	 $('.mod-fakebookBox .mod-body p').before('<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fplatform&amp;width=182&amp;height=370&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23ffffff&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:182px; height:365px;" allowTransparency="true"></iframe>').remove();
	 $('.mod-fakebookBox .mod-body img').remove();
	
	});
	
	// $(window).resize(function() {
	//  if (($(window).width() > 767) && ($(window).width() < 980)) {
	//  	$('.mod-fakebookBox iframe').width("134px");
	//  } else if (($(window).width() < 768) && ($(window).width() > 519)) {
	//  	$('.mod-fakebookBox iframe').width("448px");
	//  } else if ($(window).width() < 520) {
	//  	$('.mod-fakebookBox iframe').width("258px");
	//  }
	//  if ($(window).width() > 979) {
	//  	$('.mod-fakebookBox iframe').width("182px");
	//  }
	// });
});


/* !@file modules/mod-footer/js/mod-footer.js */
if($('body.kindermedienland').length){
	$('a.mobile-trigger').bind('click', function(ev) {
		ev.preventDefault();
		$(this).parents('.mod-footer').toggleClass('closed opened');
	});
}


/* !@file modules/mod-footerSekundaer/js/mod-footerSekundaer.js */
$(document).ready(function() {

	if ($(window).width() < 568) {
		// add click trigger
		$('body.program .mod-footer h4.mobile-trigger').on('click', function(ev) {
			ev.preventDefault();
			$(this).parents('body.program .mod-footer').toggleClass('closed opened');
		});
	}
	
});



/* !@file modules/mod-gallery/js/mod-gallery.js */
$(document).ready(function() {
    $('.flexslider-mod-gallery').flexslider({
    	startAt: 0,
		animation: 'fade',
		animationLoop: true,
		controlNav: "thumbnails",
		slideshowSpeed: 5000,
		animationSpeed: 500,
		smoothHeight: false,
		pauseOnAction: true,
		pauseOnHover: true,
		pausePlay: true,
		useCSS: false,				// # false > flickering footernav bug
		touch: true,
		prevText: 'Zurück',
		nextText: 'Weiter',
		minItems: 1,
		slideshow: false,
		start: function(s) {
		  s.find('.slide-current').html(s.currentSlide + 1);
		  s.find('.slide-count').html(s.count);

		},
		before: function(s) {
			if ($('.mod-gallery').width() > 290) {
				if (s.count > 9) {
					var row = Math.floor(((s.animatingTo) / 3))-1;
					var posY = Math.ceil(106 - (row * 70));
					if (posY > 106) posY = 106;
					s.find('.flex-control-thumbs').animate({ top: posY });
				}
			} else {
				if (s.count > 5) {
					var posX = Math.ceil((s.animatingTo-1) * 44);
					var obj = s.find('.flex-control-thumbs');
					obj.animate({ left: 15 - 44 - posX });
				}
			}
			s.find('.slide-current').html(s.animatingTo + 1);
			s.find('.slide-count').html(s.count);
		}
    });
});


/* !@file modules/mod-global-search/js/mod-global-search.js */
/*
jQuery(document).ready(function() {
    jQuery('#queryString, #gsearchInput').autocomplete({
        source: function(request, response) {
            jQuery.getJSON(
                $('#gsearchInput').parents('form').attr('action'),
                {
                    type: 1354795326,
                    tx_rsmsemanticsearch_pi1: {
                        action: 'complete',
                        searchQuery: {
                            queryString: request.term
                        }
                    }
                },
                function(data) {
                    response(data); 
                }
            );
        },
        minLength: 3
    });
});
*/


/* !@file modules/mod-glossary/js/mod-glossary.js */
$(function() {
	$(window).resize(function() {
		$('.mod-glossary:not(.faq) ul.gl_body li div').width($('.mod-glossary ul.gl_body li').width()-$('.mod-glossary ul.gl_body li div').css('margin-right'));
	});
	
	$('.mod-glossary ul.gl_nav li a').on('click', function(ev) {
		ev.preventDefault();
        var href = '#' + $(this).attr('href').split('#')[1];
		if($('h2' + href).length > 0) $("html:not(:animated),body:not(:animated)").animate({ scrollTop: $('h2' + href).offset().top } , 500);
	});
	
	$('.mod-glossary:not(.faq) ul.gl_body li > a').on('click', function(ev) {
		ev.preventDefault();
		var elem = ev.currentTarget;
		//alert($(elem).attr('class'));
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').parent().children('div').stop(true).hide('slow');
		} else {
			$('.mod-glossary ul.gl_body li a').removeClass('active');
			$('.mod-glossary ul.gl_body li div').stop(true).hide('slow', function() {
				if (!$(elem).hasClass('link-up')) {
					var tmp = 0;
					if ($('.mod-devtoolbar')) tmp = $('.mod-devtoolbar').height();
					var dest = $(elem).offset().top - tmp;
					$("html:not(:animated),body:not(:animated)").animate({ scrollTop: Number(dest)}, 500, "swing", function() {
						$(elem).addClass('active').parent().children('div').show('slow');
					});
				}
			});
		}
	});
	$('.mod-glossary:not(.faq) ul.gl_body li div a').on('click', function(ev) {
	    if($(ev.target).hasClass('link-up')) {
		    ev.preventDefault();
    		$('.mod-glossary ul.gl_body li a').removeClass('active');
            $('.mod-glossary ul.gl_body li div').stop(true).hide('slow');
		}
	});
	
	
	$('.mod-glossary:not(.faq) h2 > a').on('click', function(ev) {
		ev.preventDefault();
        if($('.gl_nav').length > 0) $("html:not(:animated),body:not(:animated)").animate({ scrollTop: $('.gl_nav').offset().top } , 500);
	});

	$('.mod-glossary.faq h2').on('click',function(ev){
		ev.preventDefault();
		if ($(this).hasClass('active')) {
            $(this).removeClass('active').next('ul.gl_body').find('div').slideUp('900');
        } else {
            $(this).addClass('active').next('ul.gl_body').find('div').slideDown('1000');
        }
	});

});


$.fn.betterSlide = function(destination) {
	var tmp = 0;
	// devbar exists?
	if ($('.mod-devtoolbar')) tmp = $('.mod-devtoolbar').height();
    var dest = $(destination).offset().top - tmp;
    // show the element and as callback scroll it into the viewport (if not animated)
    jQuery(this).show(1, function() {
        $("html:not(:animated),body:not(:animated)").animate({ scrollTop: Number(dest)}, 500, "swing");
    });
};



/* !@file modules/mod-imageGallery/js/mod-imageGallery.js */
$(document).ready(function() {
	
	$(window).load(function() {
		// get width of the first (big) image
		$('.mod-imageGallery .imageGallery-image').eq(0).each(function(){
			var imageWidth = $(this).innerWidth();
			$(this).parent().css('width', (imageWidth/16)+'em');
		});
		$('.mod-imageGallery.variant-2 .imageGallery-image').last().each(function(){
			$(this).find('img').css('float', 'none');
		});
	});
});


/* !@file modules/mod-loginAreaItem/js/mod-loginAreaItem.js */



/* !@file modules/mod-loginAreaList/js/mod-loginAreaList.js */



/* !@file modules/mod-logoandclaim/js/mod-logoandclaim.js */
$(window).load(function() {

	if ( ($(window).width() < 568) || ($('html.iphone').length) ) {
		// set div height
		var imgHeight = $('body.kml .modlogoandclaim').find('img').outerHeight();
		//$('body.kml .modlogoandclaim').css('height', imgHeight + 'px');
		$('body.kml .modlogoandclaim').attr('style','min-height:'+imgHeight+'px;');
	}

});


/* !@file modules/mod-main-menu/js/mod-main-menu.js */
if ($('.kml').length > 0) {

	$(document).ready(function() {
		$(window).trigger('resize');
	    
	});

	$(window).resize(function() {
		if ($(".kml").length > 0) {
		    if ($(window).innerWidth() > 586) {
			    
			    $('.kml .mod-main-menu').addClass("desktop-kml");
			    $('.kml .mod-main-menu').flyout();
			    
		    } else {
			    $('.kml .mod-main-menu').flyoutMobil();
			    if($('.kml .mod-main-menu').hasClass("desktop-kml") > 0) {
				    $('.kml .mod-main-menu').removeClass("desktop-kml");
			    }
		    }
	    }
	    
    });
}



/* !@file modules/mod-mediathek/js/mod-mediathek.js */
$(document).ready(function() {
	$('.mod-contactform input, .mod-contactform textarea').each(function() {
		if ($(this).attr('placeholder') && $(this).placeholder) $(this).placeholder();
	});
});

$(window).load(function() {
	// Find all YouTube videos
	var $allVideos = $("iframe[src^='http://www.youtube.com'],iframe[src^='http://www.youtube-nocookie.com']"),

	    // The element that is fluid width
	    $fluidEl = $(".article");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {
		var h = parseInt($(this).css('height').replace(/px/, '')),
			w = parseInt($(this).css('width').replace(/px/, '')),
			r = h / w;

		$(this)
			.data('aspectRatio', r)
			// and remove the hard coded width/height
			.removeAttr('height')
			.removeAttr('width');

	});

	// When the window is resized
	// (You'll probably want to debounce this)
	$(window).resize(function() {

		var newWidth = $fluidEl.width();

		// Resize all videos according to their own aspect ratio
		$allVideos.each(function() {

			var $el = $(this);
			$el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));

		});

	// Kick off one resize to fix all videos on page load
	}).resize();
});


/* !@file modules/mod-multibox/js/mod-multibox.js */
$(function() {

	//  Leere Tab-header löschen
	$('.mod-multibox .tab-header .nav-tabs li a').each(function(iIndex, sValue) {
    	if($(this).text() == ''){
			$(this).parent().html("");
		}
	});

	//  Leere Tabs ul löschen
	$('.mod-multibox .tab-pane ul').each(function(iIndex, sValue) {
    	if ($(this).text() == ''){
	    	$(this).parent().html("");
	    }
	});

	// Tab Content li => third und last setzen
	$('.mod-multibox .tab-pane ul.slides').each(function(iIndex, sValue) {
    	$(this).find('li').each(function(iIndex, sValue){
	    	// 0 3 6 9
	    	if (iIndex % 3 == 0){
		    	//alert ("li::" + iIndex);
		    	$(this).addClass("third");
		    }
		});

		// Muss das?
		$(this).find('li:last-child').each(function(iIndex, sValue){
			$(this).addClass("last");
		});
	});

	//$('.mod-multibox .mod-body').addClass("big").clone().removeClass("big").addClass("small").appendTo('.mod-multibox .tabbable');
	$('.mod-multibox .mod-body').each(function() {
		if (!$(this).is('big') && !$(this).is('small')) {
			$(this).addClass("big").clone().removeClass("big").addClass("small").appendTo($(this).parents('.mod-multibox .tabbable'));
		}
	});
	$('.mod-multibox .mod-body.big .tab-pane .slides li').wrapInner('<div class="three" />');
	$('.mod-multibox .mod-body.big .tab-pane .slides li').each(function() {
		if ($(this).is('.third')) {
			$(this).nextUntil('.third', 'li').find('.three').appendTo($(this));
		} else {
			$(this).remove();
		}
	});
	$('.mod-multibox .mod-body.big .tab-pane').flexslider({
    	startAt: 0,
		animation: 'slide',
		slideshow: false,
		animationLoop: true,
		animationSpeed: 500,
		useCSS: false,				// # false > flickering footernav bug
		touch: true,
		prevText: 'Zurück',
		nextText: 'Weiter'
	});
	$('.mod-multibox .mod-body.small .tab-pane').flexslider({
		startAt: 0,
		slideshow: false,
		animation: 'slide',
		animationLoop: true,
		animationSpeed: 500,
		useCSS: true,				// # false > flickering footernav bug
		touch: true,
		prevText: 'Zurück',
		nextText: 'Weiter'
	});
	// Tabs
	$('.mod-multibox .nav-tabs li a').on('click', function (e) {
		e.preventDefault();
		var target = e.target.getAttribute('data-class');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body:visible').children('.tab-pane').removeClass('active');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body:visible').children('.tab-pane').css('display', 'none');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body:visible').children('.tab-pane .flex-direction-nav').css('display', 'none');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body').children('.tab-pane.'+target).addClass('active');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body').children('.tab-pane.'+target).css('display', 'block');
		$(this).parents('.mod-multibox .tabbable').children('.mod-body').children('.tab-pane.'+target+' .flex-direction-nav').css('display', 'block');
		$(this).parents('.nav-tabs').children('.active').removeClass('active');
		$(this).parent().addClass('active');
  	});
});


/* !@file modules/mod-newsletterform/js/mod-newsletterform.js */
;(function() {

	$.fn.removeFormError = function() {
		$(this).find('input:not([type="submit"]), textarea').bind('click onkeydown', function() {
			$(this).css({'background-color':'#fff'});
		});
	};

	$.fn.toggleCheckboxes = function() {
		var self = $(this);

		self.each(function() {
			$(this).find('input[type="checkbox"]').change(function() {
				if (this.checked) {
					$(this).parents('ul').find('input[type="checkbox"]').attr('checked',true);
				} else {
					$(this).parents('ul').find('input[type="checkbox"]').attr('checked',false);
				}
			});
		});
	};
	

})(jQuery);


$(document).ready(function() {

	$('.mod-newsletterform').removeFormError();

	$('.checkAll').toggleCheckboxes();


});


 


/* !@file modules/mod-pagination/js/mod-pagination.js */



/* !@file modules/mod-partner/js/mod-partner.js */
$(document).ready(function() {
	
	if ($(window).width() > 568) {
		// add open-class by hover
		$('.mod-partner .partner-text').hover(function(){
			$(this).removeClass('closed');
			$(this).addClass('opend');
		},function(){
			$(this).removeClass('opend');
			$(this).addClass('closed');
		});
		// trigger close-click
		$('.mod-partner .partner-text .ico-close').on('click',function(){
			$(this).parent().addClass('closed');
		});
	}
	
});


/* !@file modules/mod-podcastteaser/js/mod-podcastteaser.js */



/* !@file modules/mod-portaleteaser/js/mod-portaleteaser.js */
$(document).ready(function() {

	// Portalslider configuration
	var portaleItems = 4;
	var portaleItemWidth = 225;

	if ($(window).width() < 568) {
		portaleItems = 1;
		portaleItemWidth = 290;
	}


    $('.portaleslider').flexslider({
    	startAt: 0,
		animation: 'slide',
		animationLoop: false,
		slideshowSpeed: 5000,
		animationSpeed: 500,
		smoothHeight: false,
		pauseOnAction: true,
		pauseOnHover: true,
		pausePlay: true,
		touch: true,
		keyboard: true,
		prevText: ' ',
		nextText: ' ',
		slideshow: false,
		move:portaleItems,
		itemWidth: portaleItemWidth,
		useCSS: false
    });


});


/* !@file modules/mod-presslist/js/mod-presslist.js */



/* !@file modules/mod-program-logoandclaim/js/mod-program-logoandclaim.js */
$(window).load(function() {

	if ( ($(window).width() < 568) || ($('html.iphone').length) ) {
		// set div height
		var imgHeight = $('body.program .sublogo').find('img').height();
		//$('body.program .sublogo').css('height', imgHeight + 'px');
		$('body.program .sublogo').attr('style','height:'+imgHeight+'px;');
	}

});


/* !@file modules/mod-program-main-menu/js/mod-program-main-menu.js */
if($('.program').length > 0) {
	;(function() {

    $.fn.flyoutMobil = function() {
        var self = $(this);

        var delay = 0;
        var toggler = $(this).find('.navbar');

        var timer;


        toggler.click(function() {
            $(this).toggleClass('hover');
        });

        self.find('.navCloseTrigger').click(function() {
            $(this).parents('.navbar').removeClass('hover');
            return false;
        });

    };

})(jQuery);

	$(document).ready(function() {
		$(window).trigger('resize');	    
	});

	$(window).resize(function() {
		if ($(".program").length > 0) {
		    if ($(window).innerWidth() > 586) {
			    
			    $('.program .mod-main-menu').addClass("desktop-program");
			    
		    } else {
			    $('.program .mod-main-menu').flyoutMobil();
			    if($('.program .mod-main-menu').hasClass("desktop-program") > 0) {
				    $('.program .mod-main-menu').removeClass("desktop-program");
			    }
		    }
	    }
    });



}



/* !@file modules/mod-publicationsCenter/js/mod-publicationsCenter.js */



/* !@file modules/mod-search/js/mod-search.js */



/* !@file modules/mod-service-menu/js/mod-service-menu.js */



/* !@file modules/mod-skipnavi/js/mod-skipnavi.js */
;(function() {

	$.fn.skipnavi = function() {
		$(this).find('a').click(function() {
			var id = $(this).data('focus');
			$('#'+id+' a').focus();
		});
	};

})(jQuery);

$(document).ready(function() {

	$('.mod-skipnavi ul').skipnavi();
});


/* !@file modules/mod-slider/js/mod-slider.js */
function flexSliderInit (){

	if($(window).width() < 568)
	{
		$('.mobile .bp .content .mod-slider').find('a[href*="/kommentieren"],a[href*="/mitmachen"]').closest('li').remove();
	}

	$('.mod-slider .flexslider').each(function(key,value) {

		if($(value).closest('.mod-slider').hasClass('teaser') || $(value).closest('.mod-slider').hasClass('text'))
			var slideshowSpeed = 8500; // Homesliderspeed
		else
			var slideshowSpeed = 7000;

	  	$(value).flexslider({
    		startAt: 0,
			animation: 'slide',
			animationLoop: true,
			slideshowSpeed: slideshowSpeed,
			animationSpeed: 600,
			smoothHeight: false,
			pauseOnAction: true,
			pauseOnHover: false,
			pausePlay: true,
			useCSS: false,				// # false > flickering footernav bug
			touch: true,
			prevText: 'Zurück',
			nextText: 'Weiter',
			pauseText: 'Pause',
			playText: 'Abspielen',
			minItems: 1,
			slideshow: true,

			start: function(s) {
				if ($(value).closest('.mod-slider').hasClass('imageline')) {
					$(s).paginate({ pagerSelector: '.flex-control-paging', activeItem: s.currentSlide, maxItems:5 });
				}
				s.find('.slide-current').html(s.currentSlide + 1);
				s.find('.slide-count').html(s.count);
				s.closest('.imageline').find('.flex-direction-nav').css('bottom', - parseInt(66 + s.closest('.well').find('.wrap-content').height()));
				s.closest('.imageline').find('.flex-control-nav').css('bottom', - parseInt(59 + s.closest('.well').find('.wrap-content').height()));
				s.closest('.imageline').find('.flex-control-nav').css({ 'margin-left' : - s.find('.flex-control-paging').width()/2, 'left': '50%'});
				s.find('.flex-prev,.flex-next')
					.on('mouseenter', function() { $(this).addClass('hover') })
					.on('mouseleave', function() { $(this).removeClass('hover') });

				if ((s.closest('.mod-slider').hasClass('teaser') || s.closest('.mod-slider').hasClass('text'))
				&& !s.closest('.mod-slider').hasClass('dossier')
				&& $(window).width() > 568
				&& s.find('.slides li').length > 1) {
					var left = Math.floor(s.find('.flex-control-nav').position().left - 25);
					//s.play();
				}

				if ($(window).width() < 569 && s.slides > 1) {
					s.find('.flex-pauseplay').css('top', s.slides.eq(s.currentSlide).height() - 23);
				} else {
					s.find('.flex-pauseplay').css('top', 'auto');
				}
				
				// center nav vertically
				var imgHeight = s.find('.flex-active-slide').find('.img').height();
				var navFromTop = (imgHeight - 35)/2;
				var pagerFromTop = (imgHeight - 33);
				s.find('.flex-direction-nav').css('top', navFromTop+'px');
				s.find('.flex-control-paging, .flex-pauseplay').css('top', pagerFromTop+'px');
			},
			before: function(s) {
				if ($(value).closest('.mod-slider').hasClass('imageline')) {
					$(s).paginate({ pagerSelector: '.flex-control-paging', activeItem: s.animatingTo, maxItems:5 });
				}
				s.closest('.imageline').find('.flex-control-nav').css({ 'margin-left' : - s.find('.flex-control-paging').width()/2, 'left': '50%'});
				s.find('.slide-current').html(s.animatingTo + 1);
				s.find('.slide-count').html(s.count);

				if (s.parent().is('.mod-slider.text') && $(window).width() > 568) {
					s.find('.flex-direction-nav').fadeOut(100);
				}
				if($(window).width() > 568) {
  				s.find('.flex-prev:not(.hover),.flex-next:not(.hover)').fadeOut(100);
				}
				

				if ($(window).width() < 569) {
					s.find('.flex-pauseplay').css('top', s.slides.eq(s.animatingTo).height() - 23);
				} else {
					//s.find('.flex-pauseplay').css('top', 'auto');
				}
			},
			after: function(s) {
				if (s.parent().is('.mod-slider.text') && $(window).width() > 568) {
			    	s.find('.flex-direction-nav').fadeIn(100);
				}
				s.find('.flex-prev:hidden,.flex-next:hidden').fadeIn(100);
				// center nav vertically
				var imgHeight = s.find('.flex-active-slide').find('.img').height();
				var navFromTop = (imgHeight - 35)/2;
				var pagerFromTop = (imgHeight - 33);
				s.find('.flex-direction-nav').css('top', navFromTop+'px');
				s.find('.flex-control-paging, .flex-pauseplay').css('top', pagerFromTop+'px');
			}
    	});

    	//imageline
		$(value).closest('.mod-slider.imageline').find('li.flex-pauseplay').append('Bild');
	});

};

$(document).ready(flexSliderInit);


/* !@file modules/mod-slider-dossier/js/mod-slider-dossier.js */



/* !@file modules/mod-targetGroupTeaser/js/mod-targetGroupTeaser.js */
$(document).ready(function() {
	
	/*
		make articleteaser cols same height
	*/
	$(window).load(function() {
		// count teaser elements
		var elements = $('.span6').find('.mod-articleteaser').size();
		var eqElements = elements/2 -1;	
		
		if(eqElements >= 0){
			// set equal height to matching row teaser
			for ( var i = 0; i <= Math.floor(eqElements); i++ ) {
				var maxHeight = -1;
				$('.span6').each(function(){
					var mod = $(this).find('.mod-articleteaser').eq(i);
					maxHeight = maxHeight > mod.height() ? maxHeight : mod.height();
				});
				$('.span6').each(function(){
					var mod = $(this).find('.mod-articleteaser').eq(i);
					mod.height(maxHeight);
				});
			}
		}
	});
	
});


/* !@file modules/mod-timeline/js/mod-timeline.js */
;(function() {

	// timeline v0.1
	$.fn.timeline = function() {
		var self = $(this);
		var scroll = $(this).find('.timeline-scroll ul');
		var item_size = scroll.find('li').length;
		var item_width = scroll.find('li').outerWidth();

		if($(window).width() > 568)
			var max_visibles = 5;
		else
			var max_visibles = 1;

		var current = 0;

		var active = scroll.find('li span.current').closest('li').index()+1;

		if (active > max_visibles) {
			current = active-max_visibles;
		}

		function registerControlls() {
			self.find('.timeline-box').append('<div class="controlls"><a href="javascript:;" class="prev">Zurück</a><a href="javascript:;" class="next">Weiter</a>');
			prev = self.find('a.prev');
			next = self.find('a.next');
		}

		function updateSliderPosition(i) {

			if (i < 0 || (i+max_visibles) > item_size)
				return;

			scroll.animate({ left : -(i*item_width) });

			current = i;

			i == 0 ? prev.hide() : prev.show();
			(i+max_visibles) == item_size ? next.hide() : next.show();
		}

		if (item_size >= max_visibles) {
			registerControlls();

			if($(window).width() > 568)
			{
				if(active > 4 && active <= (item_size-2))
					updateSliderPosition(current+2);
				else if(active == 4  || active == (item_size-1))
					updateSliderPosition(current+1);
				else
					updateSliderPosition(current);

			} else {
				updateSliderPosition(current);
			}

			next.click(function() {
				updateSliderPosition(current+1);
			});

			prev.click(function() {
				updateSliderPosition(current-1);
			});
		}
	};

})(jQuery);;



$(document).ready(function() {
	$('.mod-timeline').timeline();
});
$(window).resize(function() {
	$('.mod-timeline .controlls').remove();
	$('.mod-timeline .timeline-scroll ul').css('left','0');
	$('.mod-timeline').timeline();
});


/* !@file modules/mod-top-menu/js/mod-top-menu.js */
;(function() {
	var searchInput = $('.mod-top-menu li.search .mod-global-search').removeClass('invisible'),
		searchParent = $('.mod-top-menu li.search').addClass('closed');

	$('.mod-top-menu li.search > a').bind('click', function(ev) {
		ev.preventDefault();
		searchParent.removeClass('closed').addClass('opened');
	});
	$('.mod-top-menu li.search .mod-global-search form').bind('submit', function(ev) {
		var term = $(this).find('label input');
		console.log(term.val(), !term.val().length || term.val() == term.attr('placeholder') || term.val() == 'Suchbegriff eingeben')
		if (!term.val().length || term.val() == term.attr('placeholder') || term.val() == 'Suchbegriff eingeben') {
			ev.preventDefault();
			searchParent.removeClass('opened').addClass('closed');
		}
	});
	$(document).bind('click', function(ev) {
		if (!$(ev.target).is('.mod-top-menu li.search, .mod-top-menu li.search > a, .mod-top-menu li.search .mod-global-search, .mod-top-menu li.search .mod-global-search input')) {
			searchParent.removeClass('opened').addClass('closed');
		}
	});
	$('.mod-top-menu li.search .mod-global-search form label input').bind('click', function(ev) {
		ev.stopPropagation();
	});
	$('.mod-top-menu li.search').siblings('li.dropdown').bind('mouseenter', function() {
		searchParent.removeClass('opened').addClass('closed');
	});

	$('.mod-top-menu li.login input[placeholder]').removeAttr('placeholder');
	$('.mod-top-menu').flyout();
})(jQuery);


/* !@file modules/mod-video/js/mod-video.js */
/*! RSM-Player v1.0.0 Copyright 2013 ressourcenmangel GmbH; 2013 Brightcove, Inc. */
function createMethod(a){return function(){throw new Error('The "'+a+"\" method is not available on the playback technology's API")}}var vjs=function(a,b,c){var d;if("string"==typeof a){if(0===a.indexOf("#")&&(a=a.slice(1)),vjs.players[a])return vjs.players[a];d=vjs.el(a)}else d=a;if(!d||!d.nodeName)throw new TypeError("The element or ID supplied is not valid. (videojs)");return d.player||new vjs.Player(d,b,c)},videojs=vjs;window.videojs=window.vjs=vjs,vjs.options={techOrder:["html5","flash"],html5:{},flash:{},width:300,height:150,defaultVolume:0,children:{mediaLoader:{},posterImage:{},textTrackDisplay:{},loadingSpinner:{},bigPlayButton:{},controlBar:{}}},vjs.players={},vjs.CoreObject=vjs.CoreObject=function(){},vjs.CoreObject.extend=function(a){var b,c;a=a||{},b=a.init||a.init||this.prototype.init||this.prototype.init||function(){},c=function(){b.apply(this,arguments)},c.prototype=vjs.obj.create(this.prototype),c.prototype.constructor=c,c.extend=vjs.CoreObject.extend,c.create=vjs.CoreObject.create;for(var d in a)a.hasOwnProperty(d)&&(c.prototype[d]=a[d]);return c},vjs.CoreObject.create=function(){var a=vjs.obj.create(this.prototype);return this.apply(a,arguments),a},vjs.on=function(a,b,c){var d=vjs.getData(a);d.handlers||(d.handlers={}),d.handlers[b]||(d.handlers[b]=[]),c.guid||(c.guid=vjs.guid++),d.handlers[b].push(c),d.dispatcher||(d.disabled=!1,d.dispatcher=function(b){if(!d.disabled){b=vjs.fixEvent(b);var c=d.handlers[b.type];if(c)for(var e=c.slice(0),f=0,g=e.length;g>f&&!b.isImmediatePropagationStopped();f++)e[f].call(a,b)}}),1==d.handlers[b].length&&(document.addEventListener?a.addEventListener(b,d.dispatcher,!1):document.attachEvent&&a.attachEvent("on"+b,d.dispatcher))},vjs.off=function(a,b,c){if(vjs.hasData(a)){var d=vjs.getData(a);if(d.handlers){var e=function(b){d.handlers[b]=[],vjs.cleanUpEvents(a,b)};if(b){var f=d.handlers[b];if(f){if(!c)return e(b),void 0;if(c.guid)for(var g=0;g<f.length;g++)f[g].guid===c.guid&&f.splice(g--,1);vjs.cleanUpEvents(a,b)}}else for(var h in d.handlers)e(h)}}},vjs.cleanUpEvents=function(a,b){var c=vjs.getData(a);0===c.handlers[b].length&&(delete c.handlers[b],document.removeEventListener?a.removeEventListener(b,c.dispatcher,!1):document.detachEvent&&a.detachEvent("on"+b,c.dispatcher)),vjs.isEmpty(c.handlers)&&(delete c.handlers,delete c.dispatcher,delete c.disabled),vjs.isEmpty(c)&&vjs.removeData(a)},vjs.fixEvent=function(a){function b(){return!0}function c(){return!1}if(!a||!a.isPropagationStopped){var d=a||window.event;a={};for(var e in d)"layerX"!==e&&"layerY"!==e&&(a[e]=d[e]);if(a.target||(a.target=a.srcElement||document),a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement,a.preventDefault=function(){d.preventDefault&&d.preventDefault(),a.returnValue=!1,a.isDefaultPrevented=b},a.isDefaultPrevented=c,a.stopPropagation=function(){d.stopPropagation&&d.stopPropagation(),a.cancelBubble=!0,a.isPropagationStopped=b},a.isPropagationStopped=c,a.stopImmediatePropagation=function(){d.stopImmediatePropagation&&d.stopImmediatePropagation(),a.isImmediatePropagationStopped=b,a.stopPropagation()},a.isImmediatePropagationStopped=c,null!=a.clientX){var f=document.documentElement,g=document.body;a.pageX=a.clientX+(f&&f.scrollLeft||g&&g.scrollLeft||0)-(f&&f.clientLeft||g&&g.clientLeft||0),a.pageY=a.clientY+(f&&f.scrollTop||g&&g.scrollTop||0)-(f&&f.clientTop||g&&g.clientTop||0)}a.which=a.charCode||a.keyCode,null!=a.button&&(a.button=1&a.button?0:4&a.button?1:2&a.button?2:0)}return a},vjs.trigger=function(a,b){var c=vjs.hasData(a)?vjs.getData(a):{},d=a.parentNode||a.ownerDocument;if("string"==typeof b&&(b={type:b,target:a}),b=vjs.fixEvent(b),c.dispatcher&&c.dispatcher.call(a,b),d&&!b.isPropagationStopped()&&b.bubbles!==!1)vjs.trigger(d,b);else if(!d&&!b.isDefaultPrevented()){var e=vjs.getData(b.target);b.target[b.type]&&(e.disabled=!0,"function"==typeof b.target[b.type]&&b.target[b.type](),e.disabled=!1)}return!b.isDefaultPrevented()},vjs.one=function(a,b,c){var d=function(){vjs.off(a,b,d),c.apply(this,arguments)};d.guid=c.guid=c.guid||vjs.guid++,vjs.on(a,b,d)};var hasOwnProp=Object.prototype.hasOwnProperty;vjs.createEl=function(a,b){var c=document.createElement(a||"div");for(var d in b)hasOwnProp.call(b,d)&&(-1!==d.indexOf("aria-")||"role"==d?c.setAttribute(d,b[d]):c[d]=b[d]);return c},vjs.capitalize=function(a){return a.charAt(0).toUpperCase()+a.slice(1)},vjs._=function(a){return"undefined"!=typeof vjs_i18n&&vjs_i18n[a]?vjs_i18n[a]:a},vjs._p=function(a,b,c){return 1!=c?vjs._(b):vjs._(a)},vjs.obj={},vjs.obj.create=Object.create||function(a){function b(){}return b.prototype=a,new b},vjs.obj.each=function(a,b,c){for(var d in a)hasOwnProp.call(a,d)&&b.call(c||this,d,a[d])},vjs.obj.merge=function(a,b){if(!b)return a;for(var c in b)hasOwnProp.call(b,c)&&(a[c]=b[c]);return a},vjs.obj.deepMerge=function(a,b){var c,d,e,f;f="[object Object]",a=vjs.obj.copy(a);for(c in b)hasOwnProp.call(b,c)&&(d=a[c],e=b[c],a[c]=vjs.obj.isPlain(d)&&vjs.obj.isPlain(e)?vjs.obj.deepMerge(d,e):b[c]);return a},vjs.obj.copy=function(a){return vjs.obj.merge({},a)},vjs.obj.isPlain=function(a){return!!a&&"object"==typeof a&&"[object Object]"===a.toString()&&a.constructor===Object},vjs.bind=function(a,b,c){b.guid||(b.guid=vjs.guid++);var d=function(){return b.apply(a,arguments)};return d.guid=c?c+"_"+b.guid:b.guid,d},vjs.cache={},vjs.guid=1,vjs.expando="vdata"+(new Date).getTime(),vjs.getData=function(a){var b=a[vjs.expando];return b||(b=a[vjs.expando]=vjs.guid++,vjs.cache[b]={}),vjs.cache[b]},vjs.hasData=function(a){var b=a[vjs.expando];return!(!b||vjs.isEmpty(vjs.cache[b]))},vjs.removeData=function(a){var b=a[vjs.expando];if(b){delete vjs.cache[b];try{delete a[vjs.expando]}catch(c){a.removeAttribute?a.removeAttribute(vjs.expando):a[vjs.expando]=null}}},vjs.isEmpty=function(a){for(var b in a)if(null!==a[b])return!1;return!0},vjs.addClass=function(a,b){-1==(" "+a.className+" ").indexOf(" "+b+" ")&&(a.className=""===a.className?b:a.className+" "+b)},vjs.removeClass=function(a,b){if(-1!=a.className.indexOf(b)){for(var c=a.className.split(" "),d=c.length-1;d>=0;d--)c[d]===b&&c.splice(d,1);a.className=c.join(" ")}},vjs.TEST_VID=vjs.createEl("video"),vjs.USER_AGENT=navigator.userAgent,vjs.IS_IPHONE=/iPhone/i.test(vjs.USER_AGENT),vjs.IS_IPAD=/iPad/i.test(vjs.USER_AGENT),vjs.IS_IPOD=/iPod/i.test(vjs.USER_AGENT),vjs.IS_IOS=vjs.IS_IPHONE||vjs.IS_IPAD||vjs.IS_IPOD,vjs.IOS_VERSION=function(){var a=vjs.USER_AGENT.match(/OS (\d+)_/i);return a&&a[1]?a[1]:void 0}(),vjs.IS_ANDROID=/Android/i.test(vjs.USER_AGENT),vjs.ANDROID_VERSION=function(){var a,b,c=vjs.USER_AGENT.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i);return c?(a=c[1]&&parseFloat(c[1]),b=c[2]&&parseFloat(c[2]),a&&b?parseFloat(c[1]+"."+c[2]):a?a:null):null}(),vjs.IS_OLD_ANDROID=vjs.IS_ANDROID&&/webkit/i.test(vjs.USER_AGENT)&&vjs.ANDROID_VERSION<2.3,vjs.IS_FIREFOX=/Firefox/i.test(vjs.USER_AGENT),vjs.IS_CHROME=/Chrome/i.test(vjs.USER_AGENT),vjs.TOUCH_ENABLED="ontouchstart"in window,vjs.getAttributeValues=function(a){var b={},c=",autoplay,controls,loop,muted,default,";if(a&&a.attributes&&a.attributes.length>0)for(var d,e,f=a.attributes,g=f.length-1;g>=0;g--)d=f[g].name,e=f[g].value,("boolean"==typeof a[d]||-1!==c.indexOf(","+d+","))&&(e=null!==e?!0:!1),b[d]=e;return b},vjs.getComputedDimension=function(a,b){var c="";return document.defaultView&&document.defaultView.getComputedStyle?c=document.defaultView.getComputedStyle(a,"").getPropertyValue(b):a.currentStyle&&(c=a["client"+b.substr(0,1).toUpperCase()+b.substr(1)]+"px"),c},vjs.insertFirst=function(a,b){b.firstChild?b.insertBefore(a,b.firstChild):b.appendChild(a)},vjs.support={},vjs.el=function(a){return 0===a.indexOf("#")&&(a=a.slice(1)),document.getElementById(a)},vjs.formatTime=function(a,b){b=b||a;var c=Math.floor(a%60),d=Math.floor(a/60%60),e=Math.floor(a/3600),f=Math.floor(b/60%60),g=Math.floor(b/3600);return e=e>0||g>0?e+":":"",d=((e||f>=10)&&10>d?"0"+d:d)+":",c=10>c?"0"+c:c,e+d+c},vjs.blockTextSelection=function(){document.body.focus(),document.onselectstart=function(){return!1}},vjs.unblockTextSelection=function(){document.onselectstart=function(){return!0}},vjs.trim=function(a){return a.toString().replace(/^\s+/,"").replace(/\s+$/,"")},vjs.round=function(a,b){return b||(b=0),Math.round(a*Math.pow(10,b))/Math.pow(10,b)},vjs.createTimeRange=function(a,b){return{length:1,start:function(){return a},end:function(){return b}}},vjs.get=function(a,b,c){var d=0===a.indexOf("file:")||0===window.location.href.indexOf("file:")&&-1===a.indexOf("http");"undefined"==typeof XMLHttpRequest&&(window.XMLHttpRequest=function(){try{return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(b){}try{return new window.ActiveXObject("Msxml2.XMLHTTP")}catch(c){}throw new Error("This browser does not support XMLHttpRequest.")});var e=new XMLHttpRequest;try{e.open("GET",a)}catch(f){c(f)}e.onreadystatechange=function(){4===e.readyState&&(200===e.status||d&&0===e.status?b(e.responseText):c&&c())};try{e.send()}catch(f){c&&c(f)}},vjs.setLocalStorage=function(a,b){try{var c=window.localStorage||!1;if(!c)return;c[a]=b}catch(d){22==d.code||1014==d.code?vjs.log("LocalStorage Full (VideoJS)",d):18==d.code?vjs.log("LocalStorage not allowed (VideoJS)",d):vjs.log("LocalStorage Error (VideoJS)",d)}},vjs.getAbsoluteURL=function(a){return a.match(/^https?:\/\//)||(a=vjs.createEl("div",{innerHTML:'<a href="'+a+'">x</a>'}).firstChild.href),a},vjs.log=function(){vjs.log.history=vjs.log.history||[],vjs.log.history.push(arguments),window.console&&window.console.log(Array.prototype.slice.call(arguments))},vjs.findPosition=function(a){var b,c,d,e,f,g,h,i,j;return a.getBoundingClientRect&&a.parentNode&&(b=a.getBoundingClientRect()),b?(c=document.documentElement,d=document.body,e=c.clientLeft||d.clientLeft||0,f=window.pageXOffset||d.scrollLeft,g=b.left+f-e,h=c.clientTop||d.clientTop||0,i=window.pageYOffset||d.scrollTop,j=b.top+i-h,{left:g,top:j}):{left:0,top:0}},vjs.Component=vjs.CoreObject.extend({init:function(a,b,c){this.player_=a,this.options_=vjs.obj.copy(this.options_),b=this.options(b),this.id_=b.id||(b.el&&b.el.id?b.el.id:a.id()+"_component_"+vjs.guid++),this.name_=b.name||null,this.el_=b.el||this.createEl(),this.children_=[],this.childIndex_={},this.childNameIndex_={},this.initChildren(),this.ready(c)}}),vjs.Component.prototype.dispose=function(){if(this.trigger("dispose"),this.children_)for(var a=this.children_.length-1;a>=0;a--)this.children_[a].dispose&&this.children_[a].dispose();this.children_=null,this.childIndex_=null,this.childNameIndex_=null,this.off(),this.el_.parentNode&&this.el_.parentNode.removeChild(this.el_),vjs.removeData(this.el_),this.el_=null},vjs.Component.prototype.player_,vjs.Component.prototype.player=function(){return this.player_},vjs.Component.prototype.options_,vjs.Component.prototype.options=function(a){return void 0===a?this.options_:this.options_=vjs.obj.deepMerge(this.options_,a)},vjs.Component.prototype.el_,vjs.Component.prototype.createEl=function(a,b){return vjs.createEl(a,b)},vjs.Component.prototype.el=function(){return this.el_},vjs.Component.prototype.contentEl_,vjs.Component.prototype.contentEl=function(){return this.contentEl_||this.el_},vjs.Component.prototype.id_,vjs.Component.prototype.id=function(){return this.id_},vjs.Component.prototype.name_,vjs.Component.prototype.name=function(){return this.name_},vjs.Component.prototype.children_,vjs.Component.prototype.children=function(){return this.children_},vjs.Component.prototype.childIndex_,vjs.Component.prototype.getChildById=function(a){return this.childIndex_[a]},vjs.Component.prototype.childNameIndex_,vjs.Component.prototype.getChild=function(a){return this.childNameIndex_[a]},vjs.Component.prototype.addChild=function(a,b){var c,d,e;return"string"==typeof a?(e=a,b=b||{},d=b.componentClass||vjs.capitalize(e),b.name=e,c=new window.videojs[d](this.player_||this,b)):c=a,this.children_.push(c),"function"==typeof c.id&&(this.childIndex_[c.id()]=c),e=e||c.name&&c.name(),e&&(this.childNameIndex_[e]=c),"function"==typeof c.el&&c.el()&&this.contentEl().appendChild(c.el()),c},vjs.Component.prototype.removeChild=function(a){if("string"==typeof a&&(a=this.getChild(a)),a&&this.children_){for(var b=!1,c=this.children_.length-1;c>=0;c--)if(this.children_[c]===a){b=!0,this.children_.splice(c,1);break}if(b){this.childIndex_[a.id]=null,this.childNameIndex_[a.name]=null;var d=a.el();d&&d.parentNode===this.contentEl()&&this.contentEl().removeChild(a.el())}}},vjs.Component.prototype.initChildren=function(){var a=this.options_;if(a&&a.children){var b=this;vjs.obj.each(a.children,function(a,c){if(c!==!1){var d=function(){b[a]=b.addChild(a,c)};c.loadEvent||d()}})}},vjs.Component.prototype.buildCSSClass=function(){return""},vjs.Component.prototype.on=function(a,b){return vjs.on(this.el_,a,vjs.bind(this,b)),this},vjs.Component.prototype.off=function(a,b){return vjs.off(this.el_,a,b),this},vjs.Component.prototype.one=function(a,b){return vjs.one(this.el_,a,vjs.bind(this,b)),this},vjs.Component.prototype.trigger=function(a,b){return vjs.trigger(this.el_,a,b),this},vjs.Component.prototype.isReady_,vjs.Component.prototype.isReadyOnInitFinish_=!0,vjs.Component.prototype.readyQueue_,vjs.Component.prototype.ready=function(a){return a&&(this.isReady_?a.call(this):(void 0===this.readyQueue_&&(this.readyQueue_=[]),this.readyQueue_.push(a))),this},vjs.Component.prototype.triggerReady=function(){this.isReady_=!0;var a=this.readyQueue_;if(a&&a.length>0){for(var b=0,c=a.length;c>b;b++)a[b].call(this);this.readyQueue_=[],this.trigger("ready")}},vjs.Component.prototype.addClass=function(a){return vjs.addClass(this.el_,a),this},vjs.Component.prototype.removeClass=function(a){return vjs.removeClass(this.el_,a),this},vjs.Component.prototype.show=function(){return this.el_.style.display="block",this},vjs.Component.prototype.hide=function(){return this.el_.style.display="none",this},vjs.Component.prototype.lockShowing=function(){return this.addClass("vjs-lock-showing"),this},vjs.Component.prototype.unlockShowing=function(){return this.removeClass("vjs-lock-showing"),this},vjs.Component.prototype.disable=function(){this.hide(),this.show=function(){}},vjs.Component.prototype.width=function(a,b){return this.dimension("width",a,b)},vjs.Component.prototype.height=function(a,b){return this.dimension("height",a,b)},vjs.Component.prototype.dimensions=function(a,b){return this.width(a,!0).height(b)},vjs.Component.prototype.dimension=function(a,b,c){if(void 0!==b)return this.el_.style[a]=-1!==(""+b).indexOf("%")||-1!==(""+b).indexOf("px")?b:"auto"===b?"":b+"px",c||this.trigger("resize"),this;if(!this.el_)return 0;var d=this.el_.style[a],e=d.indexOf("px");return-1!==e?parseInt(d.slice(0,e),10):parseInt(this.el_["offset"+vjs.capitalize(a)],10)},vjs.Component.prototype.emitTapEvents=function(){var a,b,c,d;a=0,this.on("touchstart",function(){a=(new Date).getTime(),c=!0}),d=function(){c=!1},this.on("touchmove",d),this.on("touchleave",d),this.on("touchcancel",d),this.on("touchend",function(){c===!0&&(b=(new Date).getTime()-a,250>b&&this.trigger("tap"))})},vjs.Button=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b);var c=!1;this.on("touchstart",function(a){a.preventDefault(),c=!0}),this.on("touchmove",function(){c=!1});var d=this;this.on("touchend",function(a){c&&d.onClick(a),a.preventDefault()}),this.on("click",this.onClick),this.on("focus",this.onFocus),this.on("blur",this.onBlur)}}),vjs.Button.prototype.createEl=function(a,b){return b=vjs.obj.merge({className:this.buildCSSClass(),innerHTML:'<div class="vjs-control-content"><span class="vjs-control-text">'+(this.buttonText||"Need Text")+"</span></div>",role:"button","aria-live":"polite",tabIndex:0},b),vjs.Component.prototype.createEl.call(this,a,b)},vjs.Button.prototype.buildCSSClass=function(){return"vjs-control "+vjs.Component.prototype.buildCSSClass.call(this)},vjs.Button.prototype.onClick=function(){},vjs.Button.prototype.onFocus=function(){vjs.on(document,"keyup",vjs.bind(this,this.onKeyPress))},vjs.Button.prototype.onKeyPress=function(a){(32==a.which||13==a.which)&&(a.preventDefault(),this.onClick())},vjs.Button.prototype.onBlur=function(){vjs.off(document,"keyup",vjs.bind(this,this.onKeyPress))},vjs.Slider=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),this.bar=this.getChild(this.options_.barName),this.handle=this.getChild(this.options_.handleName),a.on(this.playerEvent,vjs.bind(this,this.update)),this.on("mousedown",this.onMouseDown),this.on("touchstart",this.onMouseDown),this.on("focus",this.onFocus),this.on("blur",this.onBlur),this.on("click",this.onClick),this.player_.on("controlsvisible",vjs.bind(this,this.update)),a.ready(vjs.bind(this,this.update)),this.boundEvents={}}}),vjs.Slider.prototype.createEl=function(a,b){return b=b||{},b.className=b.className+" vjs-slider",b=vjs.obj.merge({role:"slider","aria-valuenow":0,"aria-valuemin":0,"aria-valuemax":100,tabIndex:0},b),vjs.Component.prototype.createEl.call(this,a,b)},vjs.Slider.prototype.onMouseDown=function(a){a.preventDefault(),vjs.blockTextSelection(),this.boundEvents.move=vjs.bind(this,this.onMouseMove),this.boundEvents.end=vjs.bind(this,this.onMouseUp),vjs.on(document,"mousemove",this.boundEvents.move),vjs.on(document,"mouseup",this.boundEvents.end),vjs.on(document,"touchmove",this.boundEvents.move),vjs.on(document,"touchend",this.boundEvents.end),this.onMouseMove(a)},vjs.Slider.prototype.onMouseUp=function(){vjs.unblockTextSelection(),vjs.off(document,"mousemove",this.boundEvents.move,!1),vjs.off(document,"mouseup",this.boundEvents.end,!1),vjs.off(document,"touchmove",this.boundEvents.move,!1),vjs.off(document,"touchend",this.boundEvents.end,!1),this.update()},vjs.Slider.prototype.update=function(){if(this.el_){var a,b=this.getPercent(),c=this.handle,d=this.bar;if(isNaN(b)&&(b=0),a=b,c){var e=this.el_,f=e.offsetWidth,g=c.el().offsetWidth,h=g?g/f:0,i=1-h,j=b*i;a=j+h/2,c.el().style.left=vjs.round(100*j,2)+"%"}d.el().style.width=vjs.round(100*a,2)+"%"}},vjs.Slider.prototype.calculateDistance=function(a){var b,c,d,e,f,g,h,i,j;if(b=this.el_,c=vjs.findPosition(b),f=g=b.offsetWidth,h=this.handle,this.options_.vertical){if(e=c.top,j=a.changedTouches?a.changedTouches[0].pageY:a.pageY,h){var k=h.el().offsetHeight;e+=k/2,g-=k}return Math.max(0,Math.min(1,(e-j+g)/g))}if(d=c.left,i=a.changedTouches?a.changedTouches[0].pageX:a.pageX,h){var l=h.el().offsetWidth;d+=l/2,f-=l}return Math.max(0,Math.min(1,(i-d)/f))},vjs.Slider.prototype.onFocus=function(){vjs.on(document,"keydown",vjs.bind(this,this.onKeyPress))},vjs.Slider.prototype.onKeyPress=function(a){37==a.which?(a.preventDefault(),this.stepBack()):39==a.which&&(a.preventDefault(),this.stepForward())},vjs.Slider.prototype.onBlur=function(){vjs.off(document,"keydown",vjs.bind(this,this.onKeyPress))},vjs.Slider.prototype.onClick=function(a){a.stopImmediatePropagation(),a.preventDefault()},vjs.SliderHandle=vjs.Component.extend(),vjs.SliderHandle.prototype.defaultValue=0,vjs.SliderHandle.prototype.createEl=function(a,b){return b=b||{},b.className=b.className+" vjs-slider-handle",b=vjs.obj.merge({innerHTML:'<span class="vjs-control-text">'+this.defaultValue+"</span>"},b),vjs.Component.prototype.createEl.call(this,"div",b)},vjs.Player=vjs.Component.extend({init:function(a,b,c){this.tag=a,b=vjs.obj.merge(this.getTagSettings(a),b),this.cache_={},this.poster_=b.poster,this.controls_=b.controls,a.controls=!1,vjs.Component.call(this,this,b,c),this.controls()?this.addClass("vjs-controls-enabled"):this.addClass("vjs-controls-disabled"),this.one("play",function(a){var b={type:"firstplay",target:this.el_},c=vjs.trigger(this.el_,b);c||(a.preventDefault(),a.stopPropagation(),a.stopImmediatePropagation())}),this.on("ended",this.onEnded),this.on("play",this.onPlay),this.on("firstplay",this.onFirstPlay),this.on("pause",this.onPause),this.on("progress",this.onProgress),this.on("durationchange",this.onDurationChange),this.on("error",this.onError),this.on("fullscreenchange",this.onFullscreenChange),vjs.players[this.id_]=this,b.plugins&&vjs.obj.each(b.plugins,function(a,b){this[a](b)},this),this.listenForUserActivity()}}),vjs.Player.prototype.options_=vjs.options,vjs.Player.prototype.dispose=function(){this.trigger("dispose"),this.off("dispose"),vjs.players[this.id_]=null,this.tag&&this.tag.player&&(this.tag.player=null),this.el_&&this.el_.player&&(this.el_.player=null),this.stopTrackingProgress(),this.stopTrackingCurrentTime(),this.tech&&this.tech.dispose(),vjs.Component.prototype.dispose.call(this)},vjs.Player.prototype.getTagSettings=function(a){var b={sources:[],tracks:[]};if(vjs.obj.merge(b,vjs.getAttributeValues(a)),a.hasChildNodes()){var c,d,e,f,g;for(c=a.childNodes,f=0,g=c.length;g>f;f++)d=c[f],e=d.nodeName.toLowerCase(),"source"===e?b.sources.push(vjs.getAttributeValues(d)):"track"===e&&b.tracks.push(vjs.getAttributeValues(d))}return b},vjs.Player.prototype.createEl=function(){var a=this.el_=vjs.Component.prototype.createEl.call(this,"div"),b=this.tag;if(b.removeAttribute("width"),b.removeAttribute("height"),b.hasChildNodes()){var c,d,e,f,g,h;for(c=b.childNodes,d=c.length,h=[];d--;)f=c[d],g=f.nodeName.toLowerCase(),("source"===g||"track"===g)&&h.push(f);for(e=0;e<h.length;e++)b.removeChild(h[e])}return b.id=b.id||"vjs_video_"+vjs.guid++,a.id=b.id,a.className=b.className,b.id+="_html5_api",b.className="vjs-tech",b.player=a.player=this,this.addClass("vjs-paused"),this.width(this.options_.width,!0),this.height(parseInt(this.options_.height),!0),b.parentNode&&b.parentNode.insertBefore(a,b),vjs.insertFirst(b,a),a},vjs.Player.prototype.loadTech=function(a,b){this.tech?this.unloadTech():"Html5"!==a&&this.tag&&(this.el_.removeChild(this.tag),this.tag.player=null,this.tag=null),this.techName=a,this.isReady_=!1;var c=function(){this.player_.triggerReady(),this.features.progressEvents||this.player_.manualProgressOn(),this.features.timeupdateEvents||this.player_.manualTimeUpdatesOn()},d=vjs.obj.merge({source:b,parentEl:this.el_},this.options_[a.toLowerCase()]);b&&(b.src==this.cache_.src&&this.cache_.currentTime>0&&(d.startTime=this.cache_.currentTime),this.cache_.src=b.src),this.tech=new window.videojs[a](this,d),this.tech.ready(c)},vjs.Player.prototype.unloadTech=function(){this.isReady_=!1,this.tech.dispose(),this.manualProgress&&this.manualProgressOff(),this.manualTimeUpdates&&this.manualTimeUpdatesOff(),this.tech=!1},vjs.Player.prototype.manualProgressOn=function(){this.manualProgress=!0,this.trackProgress(),this.tech.one("progress",function(){this.features.progressEvents=!0,this.player_.manualProgressOff()})},vjs.Player.prototype.manualProgressOff=function(){this.manualProgress=!1,this.stopTrackingProgress()},vjs.Player.prototype.trackProgress=function(){this.progressInterval=setInterval(vjs.bind(this,function(){this.cache_.bufferEnd<this.buffered().end(0)?this.trigger("progress"):1==this.bufferedPercent()&&(this.stopTrackingProgress(),this.trigger("progress"))}),500)},vjs.Player.prototype.stopTrackingProgress=function(){clearInterval(this.progressInterval)},vjs.Player.prototype.manualTimeUpdatesOn=function(){this.manualTimeUpdates=!0,this.on("play",this.trackCurrentTime),this.on("pause",this.stopTrackingCurrentTime),this.tech.one("timeupdate",function(){this.features.timeupdateEvents=!0,this.player_.manualTimeUpdatesOff()})},vjs.Player.prototype.manualTimeUpdatesOff=function(){this.manualTimeUpdates=!1,this.stopTrackingCurrentTime(),this.off("play",this.trackCurrentTime),this.off("pause",this.stopTrackingCurrentTime)},vjs.Player.prototype.trackCurrentTime=function(){this.currentTimeInterval&&this.stopTrackingCurrentTime(),this.currentTimeInterval=setInterval(vjs.bind(this,function(){this.trigger("timeupdate")}),250)},vjs.Player.prototype.stopTrackingCurrentTime=function(){clearInterval(this.currentTimeInterval)},vjs.Player.prototype.onEnded=function(){this.options_.loop&&(this.currentTime(0),this.play())},vjs.Player.prototype.onPlay=function(){vjs.removeClass(this.el_,"vjs-paused"),vjs.addClass(this.el_,"vjs-playing")},vjs.Player.prototype.onFirstPlay=function(){this.options_.starttime&&this.currentTime(this.options_.starttime),this.addClass("vjs-has-started")},vjs.Player.prototype.onPause=function(){vjs.removeClass(this.el_,"vjs-playing"),vjs.addClass(this.el_,"vjs-paused")},vjs.Player.prototype.onProgress=function(){1==this.bufferedPercent()&&this.trigger("loadedalldata")},vjs.Player.prototype.onDurationChange=function(){this.duration(this.techGet("duration"))},vjs.Player.prototype.onError=function(a){vjs.log("Video Error",a)},vjs.Player.prototype.onFullscreenChange=function(){this.isFullScreen?this.addClass("vjs-fullscreen"):this.removeClass("vjs-fullscreen")},vjs.Player.prototype.cache_,vjs.Player.prototype.getCache=function(){return this.cache_},vjs.Player.prototype.techCall=function(a,b){if(this.tech&&!this.tech.isReady_)this.tech.ready(function(){this[a](b)});else try{this.tech[a](b)}catch(c){throw vjs.log(c),c}},vjs.Player.prototype.techGet=function(a){if(this.tech.isReady_)try{return this.tech[a]()}catch(b){throw void 0===this.tech[a]?vjs.log("Video.js: "+a+" method not defined for "+this.techName+" playback technology.",b):"TypeError"==b.name?(vjs.log("Video.js: "+a+" unavailable on "+this.techName+" playback technology element.",b),this.tech.isReady_=!1):vjs.log(b),b}},vjs.Player.prototype.play=function(){return this.techCall("play"),this},vjs.Player.prototype.pause=function(){return this.techCall("pause"),this},vjs.Player.prototype.paused=function(){return this.techGet("paused")===!1?!1:!0},vjs.Player.prototype.currentTime=function(a){return void 0!==a?(this.cache_.lastSetCurrentTime=a,this.techCall("setCurrentTime",a),this.manualTimeUpdates&&this.trigger("timeupdate"),this):this.cache_.currentTime=this.techGet("currentTime")||0},vjs.Player.prototype.duration=function(a){return void 0!==a?(this.cache_.duration=parseFloat(a),this):this.cache_.duration},vjs.Player.prototype.remainingTime=function(){return this.duration()-this.currentTime()},vjs.Player.prototype.buffered=function(){var a=this.techGet("buffered"),b=0,c=a.length-1,d=this.cache_.bufferEnd=this.cache_.bufferEnd||0;return a&&c>=0&&a.end(c)!==d&&(d=a.end(c),this.cache_.bufferEnd=d),vjs.createTimeRange(b,d)},vjs.Player.prototype.bufferedPercent=function(){return this.duration()?this.buffered().end(0)/this.duration():0},vjs.Player.prototype.volume=function(a){var b;return void 0!==a?(b=Math.max(0,Math.min(1,parseFloat(a))),this.cache_.volume=b,this.techCall("setVolume",b),vjs.setLocalStorage("volume",b),this):(b=parseFloat(this.techGet("volume")),isNaN(b)?1:b)},vjs.Player.prototype.muted=function(a){return void 0!==a?(this.techCall("setMuted",a),this):this.techGet("muted")||!1},vjs.Player.prototype.supportsFullScreen=function(){return this.techGet("supportsFullScreen")||!1},vjs.Player.prototype.requestFullScreen=function(){var a=vjs.support.requestFullScreen;return this.isFullScreen=!0,a?(vjs.on(document,a.eventName,vjs.bind(this,function(){this.isFullScreen=document[a.isFullScreen],this.isFullScreen===!1&&vjs.off(document,a.eventName,arguments.callee),this.trigger("fullscreenchange")})),this.el_[a.requestFn]()):this.tech.supportsFullScreen()?this.techCall("enterFullScreen"):(this.enterFullWindow(),this.trigger("fullscreenchange")),this},vjs.Player.prototype.cancelFullScreen=function(){var a=vjs.support.requestFullScreen;return this.isFullScreen=!1,a?document[a.cancelFn]():this.tech.supportsFullScreen()?this.techCall("exitFullScreen"):(this.exitFullWindow(),this.trigger("fullscreenchange")),this},vjs.Player.prototype.enterFullWindow=function(){this.isFullWindow=!0,this.docOrigOverflow=document.documentElement.style.overflow,vjs.on(document,"keydown",vjs.bind(this,this.fullWindowOnEscKey)),document.documentElement.style.overflow="hidden",vjs.addClass(document.body,"vjs-full-window"),this.trigger("enterFullWindow")},vjs.Player.prototype.fullWindowOnEscKey=function(a){27===a.keyCode&&(this.isFullScreen===!0?this.cancelFullScreen():this.exitFullWindow())},vjs.Player.prototype.exitFullWindow=function(){this.isFullWindow=!1,vjs.off(document,"keydown",this.fullWindowOnEscKey),document.documentElement.style.overflow=this.docOrigOverflow,vjs.removeClass(document.body,"vjs-full-window"),this.trigger("exitFullWindow")},vjs.Player.prototype.selectSource=function(a){for(var b=0,c=this.options_.techOrder;b<c.length;b++){var d=vjs.capitalize(c[b]),e=window.videojs[d];if(e.isSupported())for(var f=0,g=a;f<g.length;f++){var h=g[f];if(e.canPlaySource(h))return{source:h,tech:d}}}return!1},vjs.Player.prototype.src=function(a){if(a instanceof Array){var b,c=this.selectSource(a);c?(a=c.source,b=c.tech,b==this.techName?this.src(a):this.loadTech(b,a)):this.el_.appendChild(vjs.createEl("p",{innerHTML:'Sorry, no compatible source and playback technology were found for this video. Try using another browser like <a href="http://bit.ly/ccMUEC">Chrome</a> or download the latest <a href="http://adobe.ly/mwfN1">Adobe Flash Player</a>.'}))}else a instanceof Object?window.videojs[this.techName].canPlaySource(a)?this.src(a.src):this.src([a]):(this.cache_.src=a,this.isReady_?(this.techCall("src",a),"auto"==this.options_.preload&&this.load(),this.options_.autoplay&&this.play()):this.ready(function(){this.src(a)}));return this},vjs.Player.prototype.load=function(){return this.techCall("load"),this},vjs.Player.prototype.currentSrc=function(){return this.techGet("currentSrc")||this.cache_.src||""},vjs.Player.prototype.preload=function(a){return void 0!==a?(this.techCall("setPreload",a),this.options_.preload=a,this):this.techGet("preload")},vjs.Player.prototype.autoplay=function(a){return void 0!==a?(this.techCall("setAutoplay",a),this.options_.autoplay=a,this):this.techGet("autoplay",a)},vjs.Player.prototype.loop=function(a){return void 0!==a?(this.techCall("setLoop",a),this.options_.loop=a,this):this.techGet("loop")},vjs.Player.prototype.poster_,vjs.Player.prototype.poster=function(a){return void 0!==a&&(this.poster_=a),this.poster_},vjs.Player.prototype.controls_,vjs.Player.prototype.controls=function(a){return void 0!==a?(a=!!a,this.controls_!==a&&(this.controls_=a,a?(this.removeClass("vjs-controls-disabled"),this.addClass("vjs-controls-enabled"),this.trigger("controlsenabled")):(this.removeClass("vjs-controls-enabled"),this.addClass("vjs-controls-disabled"),this.trigger("controlsdisabled"))),this):this.controls_},vjs.Player.prototype.usingNativeControls_,vjs.Player.prototype.usingNativeControls=function(a){return void 0!==a?(a=!!a,this.usingNativeControls_!==a&&(this.usingNativeControls_=a,a?(this.addClass("vjs-using-native-controls"),this.trigger("usingnativecontrols")):(this.removeClass("vjs-using-native-controls"),this.trigger("usingcustomcontrols"))),this):this.usingNativeControls_},vjs.Player.prototype.error=function(){return this.techGet("error")},vjs.Player.prototype.ended=function(){return this.techGet("ended")},vjs.Player.prototype.userActivity_=!0,vjs.Player.prototype.reportUserActivity=function(){this.userActivity_=!0},vjs.Player.prototype.userActive_=!0,vjs.Player.prototype.userActive=function(a){return void 0!==a?(a=!!a,a!==this.userActive_&&(this.userActive_=a,a?(this.userActivity_=!0,this.removeClass("vjs-user-inactive"),this.addClass("vjs-user-active"),this.trigger("useractive")):(this.userActivity_=!1,this.tech.one("mousemove",function(a){a.stopPropagation(),a.preventDefault()}),this.removeClass("vjs-user-active"),this.addClass("vjs-user-inactive"),this.trigger("userinactive"))),this):this.userActive_},vjs.Player.prototype.listenForUserActivity=function(){var a,b,c,d,e,f;a=this.reportUserActivity,b=function(){a(),clearInterval(c),c=setInterval(vjs.bind(this,a),250)},d=function(){a(),clearInterval(c)},this.on("mousedown",b),this.on("mousemove",a),this.on("mouseup",d),this.on("keydown",a),this.on("keyup",a),this.on("touchstart",b),this.on("touchmove",a),this.on("touchend",d),this.on("touchcancel",d),e=setInterval(vjs.bind(this,function(){this.userActivity_&&(this.userActivity_=!1,this.userActive(!0),clearTimeout(f),f=setTimeout(vjs.bind(this,function(){this.userActivity_||this.userActive(!1)
}),2e3))}),250),this.on("dispose",function(){clearInterval(e),clearTimeout(f)})},function(){var a,b,c;c=document.createElement("div"),b={},void 0!==c.cancelFullscreen?(b.requestFn="requestFullscreen",b.cancelFn="exitFullscreen",b.eventName="fullscreenchange",b.isFullScreen="fullScreen"):(document.mozCancelFullScreen?(a="moz",b.isFullScreen=a+"FullScreen"):(a="webkit",b.isFullScreen=a+"IsFullScreen"),c[a+"RequestFullScreen"]&&(b.requestFn=a+"RequestFullScreen",b.cancelFn=a+"CancelFullScreen"),b.eventName=a+"fullscreenchange"),document[b.cancelFn]&&(vjs.support.requestFullScreen=b)}(),vjs.ControlBar=vjs.Component.extend(),vjs.ControlBar.prototype.options_={loadEvent:"play",children:{playToggle:{},currentTimeDisplay:{},timeDivider:{},durationDisplay:{},remainingTimeDisplay:{},progressControl:{},fullscreenToggle:{},volumeControl:{},muteToggle:{}}},vjs.ControlBar.prototype.createEl=function(){return vjs.createEl("div",{className:"vjs-control-bar"})},vjs.PlayToggle=vjs.Button.extend({init:function(a,b){vjs.Button.call(this,a,b),a.on("play",vjs.bind(this,this.onPlay)),a.on("pause",vjs.bind(this,this.onPause))}}),vjs.PlayToggle.prototype.buttonText=vjs._("Play"),vjs.PlayToggle.prototype.buildCSSClass=function(){return"vjs-play-control "+vjs.Button.prototype.buildCSSClass.call(this)},vjs.PlayToggle.prototype.onClick=function(){this.player_.paused()?this.player_.play():this.player_.pause()},vjs.PlayToggle.prototype.onPlay=function(){vjs.removeClass(this.el_,"vjs-paused"),vjs.addClass(this.el_,"vjs-playing"),this.el_.children[0].children[0].innerHTML=vjs._("Pause")},vjs.PlayToggle.prototype.onPause=function(){vjs.removeClass(this.el_,"vjs-playing"),vjs.addClass(this.el_,"vjs-paused"),this.el_.children[0].children[0].innerHTML=vjs._("Play")},vjs.CurrentTimeDisplay=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.on("timeupdate",vjs.bind(this,this.updateContent))}}),vjs.CurrentTimeDisplay.prototype.createEl=function(){var a=vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-current-time vjs-time-controls vjs-control"});return this.content=vjs.createEl("div",{className:"vjs-current-time-display",innerHTML:'<span class="vjs-control-text">'+vjs._("Current Time")+" </span>"+"0:00","aria-live":"off"}),a.appendChild(vjs.createEl("div").appendChild(this.content)),a},vjs.CurrentTimeDisplay.prototype.updateContent=function(){var a=this.player_.scrubbing?this.player_.getCache().currentTime:this.player_.currentTime();this.content.innerHTML='<span class="vjs-control-text">'+vjs._("Current Time")+" </span>"+vjs.formatTime(a,this.player_.duration())},vjs.DurationDisplay=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.on("timeupdate",vjs.bind(this,this.updateContent))}}),vjs.DurationDisplay.prototype.createEl=function(){var a=vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-duration vjs-time-controls vjs-control"});return this.content=vjs.createEl("div",{className:"vjs-duration-display",innerHTML:'<span class="vjs-control-text">'+vjs._("Duration Time")+" </span>"+"0:00","aria-live":"off"}),a.appendChild(vjs.createEl("div").appendChild(this.content)),a},vjs.DurationDisplay.prototype.updateContent=function(){this.player_.duration()&&(this.content.innerHTML='<span class="vjs-control-text">'+vjs._("Duration Time")+" </span>"+vjs.formatTime(this.player_.duration()))},vjs.TimeDivider=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b)}}),vjs.TimeDivider.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-time-divider",innerHTML:"<div><span>/</span></div>"})},vjs.RemainingTimeDisplay=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.on("timeupdate",vjs.bind(this,this.updateContent))}}),vjs.RemainingTimeDisplay.prototype.createEl=function(){var a=vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-remaining-time vjs-time-controls vjs-control"});return this.content=vjs.createEl("div",{className:"vjs-remaining-time-display",innerHTML:'<span class="vjs-control-text">'+vjs._("Remaining Time")+" </span>"+"-0:00","aria-live":"off"}),a.appendChild(vjs.createEl("div").appendChild(this.content)),a},vjs.RemainingTimeDisplay.prototype.updateContent=function(){this.player_.duration()&&this.player_.duration()&&(this.content.innerHTML='<span class="vjs-control-text">'+vjs._("Remaining Time")+" </span>"+"-"+vjs.formatTime(this.player_.remainingTime()))},vjs.FullscreenToggle=vjs.Button.extend({init:function(a,b){vjs.Button.call(this,a,b)}}),vjs.FullscreenToggle.prototype.buttonText=vjs._("Fullscreen"),vjs.FullscreenToggle.prototype.buildCSSClass=function(){return"vjs-fullscreen-control "+vjs.Button.prototype.buildCSSClass.call(this)},vjs.FullscreenToggle.prototype.onClick=function(){this.player_.isFullScreen?(this.player_.cancelFullScreen(),this.el_.children[0].children[0].innerHTML=vjs._("Fullscreen")):(this.player_.requestFullScreen(),this.el_.children[0].children[0].innerHTML=vjs._("Non-Fullscreen"))},vjs.ProgressControl=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b)}}),vjs.ProgressControl.prototype.options_={children:{seekBar:{}}},vjs.ProgressControl.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-progress-control vjs-control"})},vjs.SeekBar=vjs.Slider.extend({init:function(a,b){vjs.Slider.call(this,a,b),a.on("timeupdate",vjs.bind(this,this.updateARIAAttributes)),a.ready(vjs.bind(this,this.updateARIAAttributes))}}),vjs.SeekBar.prototype.options_={children:{loadProgressBar:{},playProgressBar:{},seekHandle:{}},barName:"playProgressBar",handleName:"seekHandle"},vjs.SeekBar.prototype.playerEvent="timeupdate",vjs.SeekBar.prototype.createEl=function(){return vjs.Slider.prototype.createEl.call(this,"div",{className:"vjs-progress-holder","aria-label":"video progress bar"})},vjs.SeekBar.prototype.updateARIAAttributes=function(){var a=this.player_.scrubbing?this.player_.getCache().currentTime:this.player_.currentTime();this.el_.setAttribute("aria-valuenow",vjs.round(100*this.getPercent(),2)),this.el_.setAttribute("aria-valuetext",vjs.formatTime(a,this.player_.duration()))},vjs.SeekBar.prototype.getPercent=function(){return this.player_.currentTime()/this.player_.duration()},vjs.SeekBar.prototype.onMouseDown=function(a){vjs.Slider.prototype.onMouseDown.call(this,a),this.player_.scrubbing=!0,this.videoWasPlaying=!this.player_.paused(),this.player_.pause()},vjs.SeekBar.prototype.onMouseMove=function(a){var b=this.calculateDistance(a)*this.player_.duration();b==this.player_.duration()&&(b-=.1),this.player_.currentTime(b)},vjs.SeekBar.prototype.onMouseUp=function(a){vjs.Slider.prototype.onMouseUp.call(this,a),this.player_.scrubbing=!1,this.videoWasPlaying&&this.player_.play()},vjs.SeekBar.prototype.stepForward=function(){this.player_.currentTime(this.player_.currentTime()+1)},vjs.SeekBar.prototype.stepBack=function(){this.player_.currentTime(this.player_.currentTime()-1)},vjs.LoadProgressBar=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.on("progress",vjs.bind(this,this.update))}}),vjs.LoadProgressBar.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-load-progress",innerHTML:'<span class="vjs-control-text">'+vjs._("Loaded: 0%")+"</span>"})},vjs.LoadProgressBar.prototype.update=function(){this.el_.style&&(this.el_.style.width=vjs.round(100*this.player_.bufferedPercent(),2)+"%")},vjs.PlayProgressBar=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b)}}),vjs.PlayProgressBar.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-play-progress",innerHTML:'<span class="vjs-control-text">'+vjs._("Progress: 0%")+"</span>"})},vjs.SeekHandle=vjs.SliderHandle.extend(),vjs.SeekHandle.prototype.defaultValue="00:00",vjs.SeekHandle.prototype.createEl=function(){return vjs.SliderHandle.prototype.createEl.call(this,"div",{className:"vjs-seek-handle"})},vjs.VolumeControl=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.tech&&a.tech.features&&a.tech.features.volumeControl===!1&&this.addClass("vjs-hidden"),a.on("loadstart",vjs.bind(this,function(){a.tech.features&&a.tech.features.volumeControl===!1?this.addClass("vjs-hidden"):this.removeClass("vjs-hidden")}))}}),vjs.VolumeControl.prototype.options_={children:{volumeBar:{}}},vjs.VolumeControl.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-volume-control vjs-control"})},vjs.VolumeBar=vjs.Slider.extend({init:function(a,b){vjs.Slider.call(this,a,b),a.on("volumechange",vjs.bind(this,this.updateARIAAttributes)),a.ready(vjs.bind(this,this.updateARIAAttributes)),setTimeout(vjs.bind(this,this.update),0)}}),vjs.VolumeBar.prototype.updateARIAAttributes=function(){this.el_.setAttribute("aria-valuenow",vjs.round(100*this.player_.volume(),2)),this.el_.setAttribute("aria-valuetext",vjs.round(100*this.player_.volume(),2)+"%")},vjs.VolumeBar.prototype.options_={children:{volumeLevel:{},volumeHandle:{}},barName:"volumeLevel",handleName:"volumeHandle"},vjs.VolumeBar.prototype.playerEvent="volumechange",vjs.VolumeBar.prototype.createEl=function(){return vjs.Slider.prototype.createEl.call(this,"div",{className:"vjs-volume-bar","aria-label":"volume level"})},vjs.VolumeBar.prototype.onMouseMove=function(a){this.player_.volume(this.calculateDistance(a))},vjs.VolumeBar.prototype.getPercent=function(){return this.player_.muted()?0:this.player_.volume()},vjs.VolumeBar.prototype.stepForward=function(){this.player_.volume(this.player_.volume()+.1)},vjs.VolumeBar.prototype.stepBack=function(){this.player_.volume(this.player_.volume()-.1)},vjs.VolumeLevel=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b)}}),vjs.VolumeLevel.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-volume-level",innerHTML:'<span class="vjs-control-text"></span>'})},vjs.VolumeHandle=vjs.SliderHandle.extend(),vjs.VolumeHandle.prototype.defaultValue="00:00",vjs.VolumeHandle.prototype.createEl=function(){return vjs.SliderHandle.prototype.createEl.call(this,"div",{className:"vjs-volume-handle"})},vjs.MuteToggle=vjs.Button.extend({init:function(a,b){vjs.Button.call(this,a,b),a.on("volumechange",vjs.bind(this,this.update)),a.tech&&a.tech.features&&a.tech.features.volumeControl===!1&&this.addClass("vjs-hidden"),a.on("loadstart",vjs.bind(this,function(){a.tech.features&&a.tech.features.volumeControl===!1?this.addClass("vjs-hidden"):this.removeClass("vjs-hidden")}))}}),vjs.MuteToggle.prototype.createEl=function(){return vjs.Button.prototype.createEl.call(this,"div",{className:"vjs-mute-control vjs-control",innerHTML:'<div><span class="vjs-control-text">'+vjs._("Mute")+"</span></div>"})},vjs.MuteToggle.prototype.onClick=function(){this.player_.muted(this.player_.muted()?!1:!0)},vjs.MuteToggle.prototype.update=function(){var a=this.player_.volume(),b=3;0===a||this.player_.muted()?b=0:.33>a?b=1:.67>a&&(b=2),this.player_.muted()?this.el_.children[0].children[0].innerHTML!=vjs._("Unmute")&&(this.el_.children[0].children[0].innerHTML=vjs._("Unmute")):this.el_.children[0].children[0].innerHTML!=vjs._("Mute")&&(this.el_.children[0].children[0].innerHTML=vjs._("Mute"));for(var c=0;4>c;c++)vjs.removeClass(this.el_,"vjs-vol-"+c);vjs.addClass(this.el_,"vjs-vol-"+b)},vjs.PosterImage=vjs.Button.extend({init:function(a,b){vjs.Button.call(this,a,b),a.poster()&&a.controls()||this.hide(),a.on("play",vjs.bind(this,this.hide))}}),vjs.PosterImage.prototype.createEl=function(){var a=vjs.createEl("div",{className:"vjs-poster",tabIndex:-1}),b=this.player_.poster();return b&&("backgroundSize"in a.style?a.style.backgroundImage='url("'+b+'")':a.appendChild(vjs.createEl("img",{src:b}))),a},vjs.PosterImage.prototype.onClick=function(){this.player().controls()&&this.player_.play()},vjs.LoadingSpinner=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),a.on("canplay",vjs.bind(this,this.hide)),a.on("canplaythrough",vjs.bind(this,this.hide)),a.on("playing",vjs.bind(this,this.hide)),a.on("seeked",vjs.bind(this,this.hide)),a.on("seeking",vjs.bind(this,this.show)),a.on("seeked",vjs.bind(this,this.hide)),a.on("error",vjs.bind(this,this.show)),a.on("waiting",vjs.bind(this,this.show))}}),vjs.LoadingSpinner.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-loading-spinner"})},vjs.BigPlayButton=vjs.Button.extend(),vjs.BigPlayButton.prototype.createEl=function(){return vjs.Button.prototype.createEl.call(this,"div",{className:"vjs-big-play-button",innerHTML:"<span></span>","aria-label":vjs._("Play video")})},vjs.BigPlayButton.prototype.onClick=function(){this.player_.play()},vjs.MediaTechController=vjs.Component.extend({init:function(a,b,c){vjs.Component.call(this,a,b,c),this.initControlsListeners()}}),vjs.MediaTechController.prototype.initControlsListeners=function(){var a,b,c,d;b=this,a=this.player();var c=function(){a.controls()&&!a.usingNativeControls()&&b.addControlsListeners()};d=vjs.bind(b,b.removeControlsListeners),this.ready(c),a.on("controlsenabled",c),a.on("controlsdisabled",d)},vjs.MediaTechController.prototype.addControlsListeners=function(){var a,b;this.on("mousedown",this.onClick),this.on("touchstart",function(a){a.preventDefault(),a.stopPropagation(),b=this.player_.userActive()}),a=function(a){a.stopPropagation(),b&&this.player_.reportUserActivity()},this.on("touchmove",a),this.on("touchleave",a),this.on("touchcancel",a),this.on("touchend",a),this.emitTapEvents(),this.on("tap",this.onTap)},vjs.MediaTechController.prototype.removeControlsListeners=function(){this.off("tap"),this.off("touchstart"),this.off("touchmove"),this.off("touchleave"),this.off("touchcancel"),this.off("touchend"),this.off("click"),this.off("mousedown")},vjs.MediaTechController.prototype.onClick=function(a){0===a.button&&this.player().controls()&&(this.player().paused()?this.player().play():this.player().pause())},vjs.MediaTechController.prototype.onTap=function(){this.player().userActive(!this.player().userActive())},vjs.MediaTechController.prototype.features={volumeControl:!0,fullscreenResize:!1,progressEvents:!1,timeupdateEvents:!1},vjs.media={},vjs.media.ApiMethods="play,pause,paused,currentTime,setCurrentTime,duration,buffered,volume,setVolume,muted,setMuted,width,height,supportsFullScreen,enterFullScreen,src,load,currentSrc,preload,setPreload,autoplay,setAutoplay,loop,setLoop,error,networkState,readyState,seeking,initialTime,startOffsetTime,played,seekable,ended,videoTracks,audioTracks,videoWidth,videoHeight,textTracks,defaultPlaybackRate,playbackRate,mediaGroup,controller,controls,defaultMuted".split(",");for(var i=vjs.media.ApiMethods.length-1;i>=0;i--){var methodName=vjs.media.ApiMethods[i];vjs.MediaTechController.prototype[vjs.media.ApiMethods[i]]=createMethod(methodName)}vjs.Html5=vjs.MediaTechController.extend({init:function(a,b,c){this.features.volumeControl=vjs.Html5.canControlVolume(),this.features.movingMediaElementInDOM=!vjs.IS_IOS,this.features.fullscreenResize=!0,vjs.MediaTechController.call(this,a,b,c);var d=b.source;d&&this.el_.currentSrc==d.src?a.trigger("loadstart"):d&&(this.el_.src=d.src),vjs.TOUCH_ENABLED&&a.options().nativeControlsForTouch!==!1&&this.useNativeControls(),a.ready(function(){this.tag&&this.options_.autoplay&&this.paused()&&(delete this.tag.poster,this.play())}),this.setupTriggers(),this.triggerReady()}}),vjs.Html5.prototype.dispose=function(){vjs.MediaTechController.prototype.dispose.call(this)},vjs.Html5.prototype.createEl=function(){var a=this.player_,b=a.tag;b&&this.features.movingMediaElementInDOM!==!1||(b?(b.player=null,a.tag=null,a.el().removeChild(b),b=b.cloneNode(!1)):b=vjs.createEl("video",{id:a.id()+"_html5_api",className:"vjs-tech"}),b.player=a,vjs.insertFirst(b,a.el()));for(var c=["autoplay","preload","loop","muted"],d=c.length-1;d>=0;d--){var e=c[d];null!==a.options_[e]&&(b[e]=a.options_[e])}return b},vjs.Html5.prototype.setupTriggers=function(){for(var a=vjs.Html5.Events.length-1;a>=0;a--)vjs.on(this.el_,vjs.Html5.Events[a],vjs.bind(this.player_,this.eventHandler))},vjs.Html5.prototype.eventHandler=function(a){this.trigger(a),a.stopPropagation()},vjs.Html5.prototype.useNativeControls=function(){var a,b,c,d,e;a=this,b=this.player(),a.setControls(b.controls()),c=function(){a.setControls(!0)},d=function(){a.setControls(!1)},b.on("controlsenabled",c),b.on("controlsdisabled",d),e=function(){b.off("controlsenabled",c),b.off("controlsdisabled",d)},a.on("dispose",e),b.on("usingcustomcontrols",e),b.usingNativeControls(!0)},vjs.Html5.prototype.play=function(){this.el_.play()},vjs.Html5.prototype.pause=function(){this.el_.pause()},vjs.Html5.prototype.paused=function(){return this.el_.paused},vjs.Html5.prototype.currentTime=function(){return this.el_.currentTime},vjs.Html5.prototype.setCurrentTime=function(a){try{this.el_.currentTime=a}catch(b){vjs.log(b,"Video is not ready. (Video.js)")}},vjs.Html5.prototype.duration=function(){return this.el_.duration||0},vjs.Html5.prototype.buffered=function(){return this.el_.buffered},vjs.Html5.prototype.volume=function(){return this.el_.volume},vjs.Html5.prototype.setVolume=function(a){this.el_.volume=a},vjs.Html5.prototype.muted=function(){return this.el_.muted},vjs.Html5.prototype.setMuted=function(a){this.el_.muted=a},vjs.Html5.prototype.width=function(){return this.el_.offsetWidth},vjs.Html5.prototype.height=function(){return this.el_.offsetHeight},vjs.Html5.prototype.supportsFullScreen=function(){return"function"!=typeof this.el_.webkitEnterFullScreen||!/Android/.test(vjs.USER_AGENT)&&/Chrome|Mac OS X 10.5/.test(vjs.USER_AGENT)?!1:!0},vjs.Html5.prototype.enterFullScreen=function(){var a=this.el_;a.paused&&a.networkState<=a.HAVE_METADATA?(this.el_.play(),setTimeout(function(){a.pause(),a.webkitEnterFullScreen()},0)):a.webkitEnterFullScreen()},vjs.Html5.prototype.exitFullScreen=function(){this.el_.webkitExitFullScreen()},vjs.Html5.prototype.src=function(a){this.el_.src=a},vjs.Html5.prototype.load=function(){this.el_.load()},vjs.Html5.prototype.currentSrc=function(){return this.el_.currentSrc},vjs.Html5.prototype.preload=function(){return this.el_.preload},vjs.Html5.prototype.setPreload=function(a){this.el_.preload=a},vjs.Html5.prototype.autoplay=function(){return this.el_.autoplay},vjs.Html5.prototype.setAutoplay=function(a){this.el_.autoplay=a},vjs.Html5.prototype.controls=function(){return this.el_.controls},vjs.Html5.prototype.setControls=function(a){this.el_.controls=!!a},vjs.Html5.prototype.loop=function(){return this.el_.loop},vjs.Html5.prototype.setLoop=function(a){this.el_.loop=a},vjs.Html5.prototype.error=function(){return this.el_.error},vjs.Html5.prototype.seeking=function(){return this.el_.seeking},vjs.Html5.prototype.ended=function(){return this.el_.ended},vjs.Html5.prototype.defaultMuted=function(){return this.el_.defaultMuted},vjs.Html5.isSupported=function(){return!!vjs.TEST_VID.canPlayType},vjs.Html5.canPlaySource=function(a){try{return!!vjs.TEST_VID.canPlayType(a.type)}catch(b){return""}},vjs.Html5.canControlVolume=function(){var a=vjs.TEST_VID.volume;return vjs.TEST_VID.volume=a/2+.1,a!==vjs.TEST_VID.volume},vjs.Html5.Events="loadstart,suspend,abort,error,emptied,stalled,loadedmetadata,loadeddata,canplay,canplaythrough,playing,waiting,seeking,seeked,ended,durationchange,timeupdate,progress,play,pause,ratechange,volumechange".split(","),vjs.IS_OLD_ANDROID&&(document.createElement("video").constructor.prototype.canPlayType=function(a){return a&&-1!=a.toLowerCase().indexOf("video/mp4")?"maybe":""}),vjs.Flash=vjs.MediaTechController.extend({init:function(a,b,c){vjs.MediaTechController.call(this,a,b,c);var d=b.source,e=b.parentEl,f=this.el_=vjs.createEl("div",{id:a.id()+"_temp_flash"}),g=a.id()+"_flash_api",h=a.options_,i=vjs.obj.merge({readyFunction:"videojs.Flash.onReady",eventProxyFunction:"videojs.Flash.onEvent",errorEventProxyFunction:"videojs.Flash.onError",autoplay:h.autoplay,preload:h.preload,loop:h.loop,muted:h.muted},b.flashVars),j=vjs.obj.merge({wmode:"opaque",bgcolor:"#000000"},b.params),k=vjs.obj.merge({id:g,name:g,"class":"vjs-tech"},b.attributes);if(d&&(i.src=encodeURIComponent(vjs.getAbsoluteURL(d.src))),vjs.insertFirst(f,e),b.startTime&&this.ready(function(){this.load(),this.play(),this.currentTime(b.startTime)}),b.iFrameMode!==!0||vjs.IS_FIREFOX)vjs.Flash.embed(b.swf,f,i,j,k);else{var l=vjs.createEl("iframe",{id:g+"_iframe",name:g+"_iframe",className:"vjs-tech",scrolling:"no",marginWidth:0,marginHeight:0,frameBorder:0});i.readyFunction="ready",i.eventProxyFunction="events",i.errorEventProxyFunction="errors",vjs.on(l,"load",vjs.bind(this,function(){var a,c=l.contentWindow;a=l.contentDocument?l.contentDocument:l.contentWindow.document,a.write(vjs.Flash.getEmbedCode(b.swf,i,j,k)),c.player=this.player_,c.ready=vjs.bind(this.player_,function(b){var c=a.getElementById(b),d=this,e=d.tech;e.el_=c,vjs.Flash.checkReady(e)}),c.events=vjs.bind(this.player_,function(a,b){var c=this;c&&"flash"===c.techName&&c.trigger(b)}),c.errors=vjs.bind(this.player_,function(a,b){vjs.log("Flash Error",b)})})),f.parentNode.replaceChild(l,f)}}}),vjs.Flash.prototype.dispose=function(){vjs.MediaTechController.prototype.dispose.call(this)},vjs.Flash.prototype.play=function(){this.el_.vjs_play()},vjs.Flash.prototype.pause=function(){this.el_.vjs_pause()},vjs.Flash.prototype.src=function(a){if(a=vjs.getAbsoluteURL(a),this.el_.vjs_src(a),this.player_.autoplay()){var b=this;setTimeout(function(){b.play()},0)}},vjs.Flash.prototype.load=function(){this.el_.vjs_load()},vjs.Flash.prototype.poster=function(){this.el_.vjs_getProperty("poster")},vjs.Flash.prototype.buffered=function(){return vjs.createTimeRange(0,this.el_.vjs_getProperty("buffered"))},vjs.Flash.prototype.supportsFullScreen=function(){return!1},vjs.Flash.prototype.enterFullScreen=function(){return!1};var api=vjs.Flash.prototype,readWrite="preload,currentTime,defaultPlaybackRate,playbackRate,autoplay,loop,mediaGroup,controller,controls,volume,muted,defaultMuted".split(","),readOnly="error,currentSrc,networkState,readyState,seeking,initialTime,duration,startOffsetTime,paused,played,seekable,ended,videoTracks,audioTracks,videoWidth,videoHeight,textTracks".split(","),createSetter=function(a){var b=a.charAt(0).toUpperCase()+a.slice(1);api["set"+b]=function(b){return this.el_.vjs_setProperty(a,b)}},createGetter=function(a){api[a]=function(){return this.el_.vjs_getProperty(a)}};!function(){var a;for(a=0;a<readWrite.length;a++)createGetter(readWrite[a]),createSetter(readWrite[a]);for(a=0;a<readOnly.length;a++)createGetter(readOnly[a])}(),vjs.Flash.isSupported=function(){return vjs.Flash.version()[0]>=10},vjs.Flash.canPlaySource=function(a){return a.type in vjs.Flash.formats?"maybe":void 0},vjs.Flash.formats={"video/flv":"FLV","video/x-flv":"FLV","video/mp4":"MP4","video/m4v":"MP4"},vjs.Flash.onReady=function(a){var b=vjs.el(a),c=b.player||b.parentNode.player,d=c.tech;b.player=c,d.el_=b,vjs.Flash.checkReady(d)},vjs.Flash.checkReady=function(a){a.el().vjs_getProperty?a.triggerReady():setTimeout(function(){vjs.Flash.checkReady(a)},50)},vjs.Flash.onEvent=function(a,b){var c=vjs.el(a).player;c.trigger(b)},vjs.Flash.onError=function(a,b){var c=vjs.el(a).player;c.trigger("error"),vjs.log("Flash Error",b,a)},vjs.Flash.version=function(){var a="0,0,0";try{a=new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(b){try{navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin&&(a=(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1])}catch(c){}}return a.split(",")},vjs.Flash.embed=function(a,b,c,d,e){var f=vjs.Flash.getEmbedCode(a,c,d,e),g=vjs.createEl("div",{innerHTML:f}).childNodes[0],h=b.parentNode;b.parentNode.replaceChild(g,b);var i=h.childNodes[0];return setTimeout(function(){i.style.display="block"},1e3),g},vjs.Flash.getEmbedCode=function(a,b,c,d){var e='<object type="application/x-shockwave-flash"',f="",g="",h="";return b&&vjs.obj.each(b,function(a,b){f+=a+"="+b+"&amp;"}),c=vjs.obj.merge({movie:a,flashvars:f,allowScriptAccess:"always",allowNetworking:"all"},c),vjs.obj.each(c,function(a,b){g+='<param name="'+a+'" value="'+b+'" />'}),d=vjs.obj.merge({data:a,width:"100%",height:"100%"},d),vjs.obj.each(d,function(a,b){h+=a+'="'+b+'" '}),e+h+">"+g+"</object>"},vjs.MediaLoader=vjs.Component.extend({init:function(a,b,c){if(vjs.Component.call(this,a,b,c),a.options_.sources&&0!==a.options_.sources.length)a.src(a.options_.sources);else for(var d=0,e=a.options_.techOrder;d<e.length;d++){var f=vjs.capitalize(e[d]),g=window.videojs[f];if(g&&g.isSupported()){a.loadTech(f);break}}}}),vjs.Player.prototype.textTracks_,vjs.Player.prototype.textTracks=function(){return this.textTracks_=this.textTracks_||[],this.textTracks_},vjs.Player.prototype.addTextTrack=function(a,b,c,d){var e=this.textTracks_=this.textTracks_||[];d=d||{},d.kind=a,d.label=b,d.language=c;var f=vjs.capitalize(a||"captions"),g=new window.videojs[f+"Track"](this,d);return e.push(g),g},vjs.Player.prototype.addTextTracks=function(a){for(var b,c=0;c<a.length;c++)b=a[c],this.addTextTrack(b.kind,b.label,b.language,b);return this},vjs.Player.prototype.showTextTrack=function(a,b){for(var c,d,e,f=this.textTracks_,g=0,h=f.length;h>g;g++)c=f[g],c.id()===a?(c.show(),d=c):b&&c.kind()==b&&c.mode()>0&&c.disable();return e=d?d.kind():b?b:!1,e&&this.trigger(e+"trackchange"),this},vjs.TextTrack=vjs.Component.extend({init:function(a,b){vjs.Component.call(this,a,b),this.id_=b.id||"vjs_"+b.kind+"_"+b.language+"_"+vjs.guid++,this.src_=b.src,this.dflt_=b["default"]||b.dflt,this.title_=b.title,this.language_=b.srclang,this.label_=b.label,this.cues_=[],this.activeCues_=[],this.readyState_=0,this.mode_=0,this.player_.on("fullscreenchange",vjs.bind(this,this.adjustFontSize))}}),vjs.TextTrack.prototype.kind_,vjs.TextTrack.prototype.kind=function(){return this.kind_},vjs.TextTrack.prototype.src_,vjs.TextTrack.prototype.src=function(){return this.src_},vjs.TextTrack.prototype.dflt_,vjs.TextTrack.prototype.dflt=function(){return this.dflt_},vjs.TextTrack.prototype.title_,vjs.TextTrack.prototype.title=function(){return this.title_},vjs.TextTrack.prototype.language_,vjs.TextTrack.prototype.language=function(){return this.language_},vjs.TextTrack.prototype.label_,vjs.TextTrack.prototype.label=function(){return this.label_},vjs.TextTrack.prototype.cues_,vjs.TextTrack.prototype.cues=function(){return this.cues_},vjs.TextTrack.prototype.activeCues_,vjs.TextTrack.prototype.activeCues=function(){return this.activeCues_},vjs.TextTrack.prototype.readyState_,vjs.TextTrack.prototype.readyState=function(){return this.readyState_},vjs.TextTrack.prototype.mode_,vjs.TextTrack.prototype.mode=function(){return this.mode_},vjs.TextTrack.prototype.adjustFontSize=function(){this.el_.style.fontSize=this.player_.isFullScreen?100*1.4*(screen.width/this.player_.width())+"%":""},vjs.TextTrack.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-"+this.kind_+" vjs-text-track","aria-live":"descriptions"==this.kind_?"live":"off"})},vjs.TextTrack.prototype.show=function(){this.activate(),this.mode_=2,vjs.Component.prototype.show.call(this)},vjs.TextTrack.prototype.hide=function(){this.activate(),this.mode_=1,vjs.Component.prototype.hide.call(this)},vjs.TextTrack.prototype.disable=function(){2==this.mode_&&this.hide(),this.deactivate(),this.mode_=0},vjs.TextTrack.prototype.activate=function(){0===this.readyState_&&this.load(),0===this.mode_&&(this.player_.on("timeupdate",vjs.bind(this,this.update,this.id_)),this.player_.on("ended",vjs.bind(this,this.reset,this.id_)),("captions"===this.kind_||"descriptions"===this.kind_)&&this.player_.getChild("textTrackDisplay").addChild(this))},vjs.TextTrack.prototype.deactivate=function(){this.player_.off("timeupdate",vjs.bind(this,this.update,this.id_)),this.player_.off("ended",vjs.bind(this,this.reset,this.id_)),this.reset(),this.player_.getChild("textTrackDisplay").removeChild(this)},vjs.TextTrack.prototype.load=function(){0===this.readyState_&&(this.readyState_=1,vjs.get(this.src_,vjs.bind(this,this.parseCues),vjs.bind(this,this.onError)))},vjs.TextTrack.prototype.onError=function(a){this.error=a,this.readyState_=3,this.trigger("error")},vjs.TextTrack.prototype.parseCues=function(a){for(var b,c,d,e,f=a.split("\n"),g="",h=1,i=f.length;i>h;h++)if(g=vjs.trim(f[h])){for(-1==g.indexOf("-->")?(e=g,g=vjs.trim(f[++h])):e=this.cues_.length,b={id:e,index:this.cues_.length},c=g.split(" --> "),b.startTime=this.parseCueTime(c[0]),b.endTime=this.parseCueTime(c[1]),d=[];f[++h]&&(g=vjs.trim(f[h]));)d.push(g);b.text=d.join("<br/>"),this.cues_.push(b)}this.readyState_=2,this.trigger("loaded")},vjs.TextTrack.prototype.parseCueTime=function(a){var b,c,d,e,f,g=a.split(":"),h=0;return 3==g.length?(b=g[0],c=g[1],d=g[2]):(b=0,c=g[0],d=g[1]),d=d.split(/\s+/),e=d.splice(0,1)[0],e=e.split(/\.|,/),f=parseFloat(e[1]),e=e[0],h+=3600*parseFloat(b),h+=60*parseFloat(c),h+=parseFloat(e),f&&(h+=f/1e3),h},vjs.TextTrack.prototype.update=function(){if(this.cues_.length>0){var a=this.player_.currentTime();if(void 0===this.prevChange||a<this.prevChange||this.nextChange<=a){var b,c,d,e,f=this.cues_,g=this.player_.duration(),h=0,i=!1,j=[];for(a>=this.nextChange||void 0===this.nextChange?e=void 0!==this.firstActiveIndex?this.firstActiveIndex:0:(i=!0,e=void 0!==this.lastActiveIndex?this.lastActiveIndex:f.length-1);;){if(d=f[e],d.endTime<=a)h=Math.max(h,d.endTime),d.active&&(d.active=!1);else if(a<d.startTime){if(g=Math.min(g,d.startTime),d.active&&(d.active=!1),!i)break}else i?(j.splice(0,0,d),void 0===c&&(c=e),b=e):(j.push(d),void 0===b&&(b=e),c=e),g=Math.min(g,d.endTime),h=Math.max(h,d.startTime),d.active=!0;if(i){if(0===e)break;e--}else{if(e===f.length-1)break;e++}}this.activeCues_=j,this.nextChange=g,this.prevChange=h,this.firstActiveIndex=b,this.lastActiveIndex=c,this.updateDisplay(),this.trigger("cuechange")}}},vjs.TextTrack.prototype.updateDisplay=function(){for(var a=this.activeCues_,b="",c=0,d=a.length;d>c;c++)b+='<span class="vjs-tt-cue">'+a[c].text+"</span>";this.el_.innerHTML=b},vjs.TextTrack.prototype.reset=function(){this.nextChange=0,this.prevChange=this.player_.duration(),this.firstActiveIndex=0,this.lastActiveIndex=0},vjs.CaptionsTrack=vjs.TextTrack.extend(),vjs.CaptionsTrack.prototype.kind_="captions",vjs.DescriptionsTrack=vjs.TextTrack.extend(),vjs.DescriptionsTrack.prototype.kind_="descriptions",vjs.TextTrackDisplay=vjs.Component.extend({init:function(a,b,c){vjs.Component.call(this,a,b,c),a.options_.tracks&&a.options_.tracks.length>0&&this.player_.addTextTracks(a.options_.tracks)}}),vjs.TextTrackDisplay.prototype.createEl=function(){return vjs.Component.prototype.createEl.call(this,"div",{className:"vjs-text-track-display"})},vjs.TextTrackToggle=vjs.Button.extend({track:{},init:function(a,b){vjs.Button.call(this,a,b);for(var c=this.player_.textTracks(),d=!1,e=0,f=c.length;f>e;e++)c[e].kind()===this.kind_&&(this.track=c[e],d=!0,this.track.kind()==this.kind_&&2==this.track.mode()&&(this.off=!1));0==d&&this.hide()}}),vjs.TextTrackToggle.prototype.onClick=function(){vjs.Button.prototype.onClick.call(this),this.off?(this.off=!1,this.player_.showTextTrack(this.track.id_,this.track.kind()),this.el_.setAttribute("aria-label",this.labelDisable),this.el_.setAttribute("title",this.labelDisable),this.addClass("vjs-enabled")):(this.off=!0,this.player_.showTextTrack(null,this.track.kind()),this.el_.setAttribute("aria-label",this.labelEnable),this.el_.setAttribute("title",this.labelEnable),this.removeClass("vjs-enabled"))},vjs.CaptionsToggle=vjs.TextTrackToggle.extend({init:function(a,b,c){vjs.TextTrackToggle.call(this,a,b,c),this.el_.setAttribute("aria-label",this.labelEnable),this.el_.setAttribute("title",this.labelEnable),this.el_.setAttribute("data-label",this.buttonText)}}),vjs.CaptionsToggle.prototype.off=!0,vjs.CaptionsToggle.prototype.kind_="captions",vjs.CaptionsToggle.prototype.buttonText=vjs._("Captions"),vjs.CaptionsToggle.prototype.labelEnable=vjs._("Enable captions"),vjs.CaptionsToggle.prototype.labelDisable=vjs._("Disable captions"),vjs.CaptionsToggle.prototype.buildCSSClass=function(){return"vjs-captions-button "+vjs.Button.prototype.buildCSSClass.call(this)
},vjs.DescriptionsToggle=vjs.TextTrackToggle.extend({init:function(a,b,c){vjs.TextTrackToggle.call(this,a,b,c),this.el_.setAttribute("aria-label",this.labelEnable),this.el_.setAttribute("title",this.labelEnable),this.el_.setAttribute("data-label",this.buttonText)}}),vjs.DescriptionsToggle.prototype.off=!0,vjs.DescriptionsToggle.prototype.kind_="descriptions",vjs.DescriptionsToggle.prototype.buttonText=vjs._("Audio description"),vjs.DescriptionsToggle.prototype.labelEnable=vjs._("Enable audio description"),vjs.DescriptionsToggle.prototype.labelDisable=vjs._("Disable audio description"),vjs.DescriptionsToggle.prototype.buildCSSClass=function(){return"vjs-descriptions-button "+vjs.Button.prototype.buildCSSClass.call(this)},vjs.obj.merge(vjs.ControlBar.prototype.options_.children,{DescriptionsToggle:{},CaptionsToggle:{}}),vjs.autoSetup=function(){var a,b,c=document.getElementsByTagName("video");if(c&&c.length>0)for(var d=0,e=c.length;e>d;d++){if(a=c[d],!a||!a.getAttribute){vjs.autoSetupTimeout(1);break}void 0===a.player&&(b=videojs(a,{}))}else vjs.windowLoaded||vjs.autoSetupTimeout(1)},vjs.autoSetupTimeout=function(a){setTimeout(vjs.autoSetup,a)},vjs.one(window,"load",function(){vjs.windowLoaded=!0}),vjs.autoSetupTimeout(1);


/* !@file modules/mod-votingteaser/js/mod-votingteaser.js */
/*$(document).ready(function(){
	if ($('#mod-votingteaser').length > 0) {
	
		//console.log("Visualize!");
	
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);
	
		function drawChart() {
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Antwort');
			data.addColumn('number', 'Prozent');
		
			for(i=0; i < chartData.length; i++) {
				data.addRow([chartData[i]['text'],chartData[i]['value']]);
			}
		
			var options = {
			  backgroundColor: { fill:'transparent' },
			  legend: {position: 'none'},
			  chartArea:{left:0,top:0,width:"100%"},
			  tooltip: { text: 'value', showColorCode: false},
			  fontSize: '13',
			  colors: ['#FF4D48', '#FF6464', '#FF7D7D', '#FF9797', '#FFB3B3']
			};
			
			var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			chart.draw(data, options);
		}
	}
});*/

$(document).ready(function(){

  if ($('#mod-votingteaser').length > 0) {

	  var data = new Array();

    for(i=0; i<chartData.length; i++) {

      data[i] = new Array();
      data[i]=[chartData[i]['text'], chartData[i]['value']];
    
    }

    var plot1 = jQuery.jqplot ('chart1', [data], 
    {
      seriesDefaults: {
        
        renderer: jQuery.jqplot.PieRenderer,
        rendererOptions: {
          showDataLabels: true,
          seriesColors: chartColors
        }
      },
      legend: { show: false, location: 's' }
    });
  }
});


/* !@file modules/mod-votingteaser2/js/mod-votingteaser2.js */
if ($('#mod-votingteaser2').length > 0){

	$(".styled input:checked").parent().addClass("checked");
		
	$(document).on("change", ".styled input", function() {
		var $this = $(this);
		var form = $this.closest("form");
		form.find("input[name='"+$this.attr("name")+"']").parent().removeClass("checked");
		$this.parent().addClass("checked");
	});

}

