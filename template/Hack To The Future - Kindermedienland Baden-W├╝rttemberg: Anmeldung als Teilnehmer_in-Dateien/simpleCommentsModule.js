
/*
 * I know, could be done smarter with a fn and options,
 * but it was too late ...
 */
jQuery(document).ready(function() {
	jQuery('.jsSimpleComment').each(
		function() {
		// initial variables
		var $this = jQuery(this),
            triggers = $('.jsSimpleCommentsVote',$this),
            trigger = triggers.find('.jsSimpleCommentsVoteTrigger'),
            uid = trigger.data('uid'),
            voteUps = $('.jsSimpleCommentVoteUps',$this),
            voteDowns = $('.jsSimpleCommentVoteDowns',$this),
            eID = 'tx_rsmsimplecomments_ajax';
		// add styles and click function...
            trigger
			.css ({'cursor':'pointer'})
			.click(function(){
			// do ajax request and replace content
                var vote = jQuery(this).data('vote');
                    triggers.hide().off('click');
				jQuery.ajax({
					url: 'index.php',
					type: 'GET',
					dataType: 'html',
					data: {eID:eID,uid:uid,vote:vote,clicked:'1'},
					success: function(data) {
                        triggers
						.html(data)
                        .show()
						.off('click')
						.css ({'cursor':''});
                        switch(vote) {
                            case 'up':
                                voteUps.text( parseInt(voteUps.text()) + 1);
                                break;
                            case 'down':
                                voteDowns.text( parseInt(voteDowns.text()) + 1);
                                break;
                        }
					}
				});
			});
		// do ajax request to check if already clicked
			jQuery.ajax({
				url: 'index.php',
				type: 'GET',
				dataType: 'html',
				data: {eID:eID,uid:uid,clicked:''},
				success: function(data) {
					if(data) {
                        triggers
						.html(data)
						.off('click')
						.css ({'cursor':''});
					}
				}
			});
		});

});