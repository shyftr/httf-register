## Installation

#### Installation on ARCH Linux (only v1.0 tested)
```
$ sudo pacman -S git p7zip python-pip firefox-esr python3-pip

$ git clone -b 'v1.0' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Installation on Fedora (!untested!)
```
$ sudo dnf -y install git firefox-esr

$ sudo dnf install p7zip p7zip-plugins

$ git clone -b 'v1.0' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Installation on openSUSE (!untested!)
```
$ sudo zypper in git firefox-esr

$ sudo zypper install p7zip

$ git clone -b 'v1.0' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Installation on RHEL/CentOS (!untested!)
```
$ sudo sudo yum install git firefox-esr

$ sudo yum install p7zip p7zip-plugins

$ git clone -b 'v1.0' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```
