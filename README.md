<img src="https://gitlab.com/mindcrew-dev/httf-register/raw/master/design/python3.7-blue.png" hight="10" width="101">

# //  HTTF  ||  Register script  //

This Python 3 script will make the regestration process for [HackToTheFuture](https://www.hacktothefuture.de/) a lot easier.

<img src="https://gitlab.com/mindcrew-dev/httf-register/raw/master/design/httf_gen_logo_600x600.png" height="450" width="450">

## Information
Once you start the setup.py, you'll be asked a pile of questions. At the end of this question "tornado" you can decide if your information should be stored in a AES256 encrypted 7zip file. If yes, you'll be asked for a password to encrypt the .db file, so no one except you can edit or see YOUR data. We recommend to encrypt your personal data for security reasons.


## Installation

#### Installation on Linux (Ubuntu / Debian / Linux Mint / Kali)
```
$ sudo apt install git p7zip-full p7zip-rar firefox python-pip python3 python3-pip -y

$ git clone -b 'v3.2' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```
If this won't work for you, try an older version.

#### Installation on Linux (Most stable version) (Ubuntu / Debian / Linux Mint / Kali)
```
$ sudo apt install git p7zip-full p7zip-rar firefox python-pip python3 python3-pip -y

$ git clone -b 'v1.0' https://gitlab.com/mindcrew-dev/httf-register.git

$ cd httf-register

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Installation  on macOS

* Of course you'll need Firefox, you can download it from [here](https://www.mozilla.org/de/firefox/download/thanks/).
* Download the [zip](https://gitlab.com/mindcrew-dev/httf-register/-/archive/v1.0/httf-register-v1.0.zip) from our GitLab page.
* unzip the .zip file (normally you should just double click on it, and it'll extract itself)
* open a Terminal (Keys: cmd + space and type in 'terminal'. Press enter.)
* now run the commands below.
```
$ cd Downloads

$ mv httf-register-master ../Documents/

$ rm httf-register-master.zip

$ cd ../Documents/httf-register-master/

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Installation  on Windows (only 64bit)

* Of course you'll need Firefox, you can download it from [here](https://www.mozilla.org/de/firefox/download/thanks/).
* Download the [zip](https://gitlab.com/mindcrew-dev/httf-register/-/archive/v1.0/httf-register-v1.0.zip) from our GitLab page.
* unzip the .zip file
* open a cmd (Windowskey + r ,type in cmd and press enter.)
* now run the commands below.
```
$ cd Downloads

$ move httf-register-master ../Documents/

$ del httf-register-master.zip

$ cd ../Documents/httf-register-master/

$ pip3 install -r requirements.txt

$ python3 setup.py

```

#### Other operating systems (e.g. ARCH / Fedora)

* If you want to install this project on a other operating system, check out our [Installation on other operating systems](INSTALLATION_ON_OTHER_OS.md) guide.


## How To Use
```
$ python3 main.py
```

## Built With

* [Python 3](https://www.python.org/) - Programming language
* [Selenium](https://www.seleniumhq.org/) - Brower automation
* [GeckoDriver](https://github.com/mozilla/geckodriver/) - Browser driver for Firefox

## Authors

* **Sascha S.** - [Mv0sKff](https://gitlab.com/Mv0sKff)
* **Maris B.** - [TheProgramming_M](https://gitlab.com/TheProgramming_M)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## HTTF ACC GENERATOR

```
 __      __ ____________ ____________ ________
|  |    |  |_____   ____|_____   ____|   _____|
|  |____|  |    |  |         |  |    |  |_____
|   ____   |    |  |         |  |    |  ______|
|  |    |  |    |  |         |  |    |  |
|__|    |__|    |__|         |__|    |__|
        ____     _________   _________
       |    |   |   ______| |   ______|
      |  __  |  |  |        |  |
      | |__| |  |  |        |  |
     |   __   | |  |______  |  |______
     |__|  |__| |_________| |_________|
 ______ _____ _     _ _____ ______   __ ______ ______ ______
|  ____|  ___| \   | |  ___|  __  | |  |___  _|      |  __  |
| | ___| |___|   \ | | |___| |__| || __ | | | |  __  | |__| |
| ||_  |  ___|  \    |  ___|  __ | ||__|| | | | |__| |  __ |
| |__| | |___| | \   | |___| |  \ \| __  || | |      | |  \ \
|______|_____|_|  \|_|_____|_|   \|_|  |_||_| |______|_|   \_\
```

## Demo
 <img src="https://gitlab.com/mindcrew-dev/httf-register/raw/master/design/httf-register-demo.gif" alt="Demo.gif">
