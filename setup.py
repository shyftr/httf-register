#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, platform, urllib.request, time, warnings, sqlite3, itertools, threading, time, sys, hashlib, random, struct
from zipfile import ZipFile


BLUE, RED, YELLOW, GREEN, END = '\33[94m', '\033[91m', '\33[93m', '\033[1;32m', '\033[0m'

#URLS
url_geckodriver_linux = 'https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz'
url_geckodriver_macos = 'https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-macos.tar.gz'
url_geckodriver_win = 'https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-win64.zip'
url_7zip_win = 'https://www.7-zip.org/a/7z1900-x64.exe'


#Set variables for Operating system
opsys = platform.system()
print("Run setup for " + opsys)

done = False

forename = "undefined"
lastname = "undefined"
street = "undefined"
house_number = "undefined"
plz = "undefined"
city = "undefined"
email = "undefined"
gender = "undefined"
birthday = "undefined"
phone_number = "undefined"
allergic = "undefined"
parent_name = "undefined"
phone_number_parent = "undefined"
email_parent = "undefined"
db_have_programmed = "undefined"
programming_language = "undefined"
db_hardware = "undefined"
which_skills = "undefined"
what_try = "undefined"
keen_on = "undefined"
db_laptop = "undefined"



if os.path.isfile('./data.db') is True:
   os.system('rm data.db')
   if platform.system() is "Windows":
      os.system("del data.db")



def LinuxInstall():
    #Run animation
    done = False
    def animate():
        for c in itertools.cycle(['|', '/', '-', '\\']):
            if done == True:
                break
            sys.stdout.write('\rloading ' + c)
            sys.stdout.flush()
            time.sleep(0.1)
        sys.stdout.write('\rDone!     \n')

    t = threading.Thread(target=animate)
    t.start()


    print()
    print("Download Geckodriver version 0.26.0 from: \n" + url_geckodriver_linux)
    print("-------------------------------------------------------------------------")

    #Download Geckodriver for Linux
    if os.path.isfile('./geckodriver') is True:
        result = os.popen('echo "6590e3b9d9bf292c8df50b6df5bcf8a5191d999f9e48f68aa2055eb5746b2c05 *geckodriver" | shasum -a 256 --check')
        print(result)
        if (str.__contains__("OK", str(result))) == True:
            print("aaa")
            os.system('rm geckodriver')
            urllib.request.urlretrieve(url_geckodriver_linux, "geckodriver-v0.26.0-linux64.tar.gz")
            os.system('tar -zxvf geckodriver-v0.26.0-linux64.tar.gz')
            os.system('rm geckodriver-v0.26.0-linux64.tar.gz')
    else:
        urllib.request.urlretrieve(url_geckodriver_linux, "geckodriver-v0.26.0-linux64.tar.gz")
        os.system('tar -zxvf geckodriver-v0.26.0-linux64.tar.gz')
        os.system('rm geckodriver-v0.26.0-linux64.tar.gz')


    if os.path.isfile('./data.db') is True:
        os.system('rm data.db')

    if os.path.isfile('./geckodriver.log') is True:
        os.system('rm geckodriver.log')

    done = True #Stop animation



def macInstall():
    print("\nDownload Geckodriver version 0.24.0 from " + url_geckodriver_macos)

    #Run animation
    done = False
    def animate():
        for c in itertools.cycle(['|', '/', '-', '\\']):
            if done == True:
                break
            sys.stdout.write('\rloading ' + c)
            sys.stdout.flush()
            time.sleep(0.1)
        sys.stdout.write('\rDone!     \n')

    t = threading.Thread(target=animate)
    t.start()

    os.system("clear")
    print("Please press RETURN if asked.")
    print()
    time.sleep(0.7)
    os.system('/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"')
    os.system("brew install wget")
    os.system("wget " + url_geckodriver_macos)
    os.system('tar -zxvf geckodriver-v0.24.0-macos.tar.gz')
    os.system('rm geckodriver-v0.24.0-macos.tar.gz')
    done = True #Stop animation



def WindowsInstall():
        time.sleep(0.2)
        print()
        print("Attention! Installation on Widows wasn't testet yet!")
        print("Download Geckodriver version 0.24.0 from " + url_geckodriver_win)
        done = False

        #Run animation
        done = False
        def animate():
            for c in itertools.cycle(['|', '/', '-', '\\']):
                if done == True:
                    break
                sys.stdout.write('\rloading ' + c)
                sys.stdout.flush()
                time.sleep(0.1)
            sys.stdout.write('\rDone!     \n')

        t = threading.Thread(target=animate)
        t.start()

        urllib.request.urlretrieve(url_geckodriver_win, "geckodriver-v0.24.0-win64.zip")
        if os.path.isfile(r'"C:\Program Files\7-Zip\7z.exe"') is "False":
            print("Installing 7zip...")
            print("Please allow installing 7zip")
            urllib.request.urlretrieve(url_7zip_win, "7z1900-x64.exe")
            os.system("start /wait 7z1900-x64.exe /S ")
        else:
            print("7zip already installed")
        with ZipFile('geckodriver-v0.24.0-win64.zip', 'r') as zipObj:
            # Extract all the contents of zip file in current directory
            zipObj.extractall()
        os.system('del geckodriver-v0.24.0-win64.zip')
        os.system("del 7z1900-x64.exe")
        done = True



def Q(text, inputmark):
    print(text)
    print("Drücke dannach ENTER!")
    ivar = input(inputmark)
    #print(var)
    print(ivar)
    print("--------------------------------------------------------------------------------")
    return ivar

def Qgender():
    #for x in range(100):
    print("Bitte gebe deine Geschlecht ein (m,w oder d).\nm = Männlich\nw = Weiblich\nd = Divers")
    print("Drücke dannach ENTER!")
    ivar = input("[m/w/d]: ")
    print(ivar)
    gender = ivar
    print("--------------------------------------------------------------------------------")
    if ivar.lower() == "w" or ivar.lower() == "m" or ivar.lower() == "d":
        gender = ivar.lower()
        return gender
        #break
    else:
        print("ERROR invalid input! Try again!")
        print("--------------------------------------------------------------------------------")
        print("Too many trys!")
        print()
        sys.exit("ERROR: Too many trys!")

#def Qtshirt():
#    #for x in range(100):
#    print("Bitte gebe deine T-Shirt größe ein. (XS, S, M, L, XL, XXL oder XXXL).")
#    print("Drücke dannach ENTER!")
#    ivar = input("[XS/S/M/L/XL/XXL/XXXL]: ")
#    print(ivar)
#    print(ivar.upper())
#    tsize = ivar
#    print("--------------------------------------------------------------------------------")
#    if ivar.upper() == "XS" or ivar.upper() == "S" or ivar.upper() == "M" or ivar.upper() == "L" or ivar.upper() == "XL" or ivar.upper() == "XXL" or ivar.upper() == "XXXL":
#        tsize = ivar.upper()
#        return tsize
#        #break
#    else:
#        print("ERROR invalid input! Try again!")
#        print("--------------------------------------------------------------------------------")
#        print("Too many trys!")
#        print()
#        sys.exit("ERROR: Too many trys!")

def QuestionDialogAndDoSqllite():
    connection = sqlite3.connect("data.db")
    time.sleep(0.8)
    #os.system("clear")
    print("Drücke nach jeder Antwort ENTER, außer es steht etwas anderes da!")

    forename = Q("Bitte gebe deinen Vornamen ein.", "Vorname: ")
    lastname = Q("Bitte gebe deinen Nachnamen ein.", "Nachname: ")
    street = Q("Bitte gebe deine Straße ein! NICHT DIE HAUSNUMMER", "Straße: ")
    house_number = Q("Bitte gebe deine Hausnummer ein.", "Hausnummer: ")
    plz = Q("Bitte gebe deine Postleitzahl ein.", "PLZ: ")
    city = Q("Bitte gebe deine Stadt ein.", "Stadt: ")
    email = Q("Bitte gebe deine E-Mail ein.", "E-Mail: ")
#    gender = Q("Bitte gebe deine Geschlecht ein (m,w oder d).\nm = Männlich\nw = Weiblich\nd = Divers", "[m/w/d]: ")
    gender = Qgender()
    #tsize = Qtshirt()

    birthday = Q("Bitte gebe dein Geburtsdatum ein (mit Punkten: dd.mm.YYYY).", "Geburtsdatum: ")
    phone_number = Q("Bitte gebe deine Telefon/Handynummer ein (Beispiel: +4917640284730).", "Telefon/Handynummer: ")
    allergic = Q("Bitte gebe Lebensmittel an, die du nicht essen darfst oder kannst (falls vorhanden).\nWenn du alles essen kannst/darfs drücke einfach ENTER", "Lebensmittel: ")
    parent_name = Q("Bitte gebe den Vor und Nachnamen eines Erziehungsberechtigten an.", "Name des Erziehungsberechtigten: ")
    phone_number_parent = Q("Bitte gebe die Telefonnummer eines Erziehungsberechtigten an (Beispiel: +490000000000).", "Nummer eines Erziehungsberechtigten: ")
    email_parent = Q("Bitte gebe die E-Mail eines Erziehungsberechtigten an (Beispiel: httf.fan@gmail.com).","E-Mail eines Erziehungsberechtigten: ")
    db_have_programmed = Q("Bitte gebe an, ob du schonmal programiert hast.\nSchreibe 'yes' für ja\noder 'no' für nein", "Hast du schonmal programmiert?: ")
    programming_language = Q("Bitte gebe an, mit welchen Programmiersprachen du schonmal programmiert hast (falls vorhanden).", "Programmiersprachen: ")
    db_hardware = Q("Bitte gebe an, mit welcher Hardware du schonmal gearbeitet hast (falls vorhanden).\n1 = Arduino\n2 = Raspberry Pi\n3 = Calliope Mini\n4 = B-O-B-3\nWenn du von den aufgelisteten Geräten mehrere benutzt hast, schreibe es so: 134\nWenn du zum Beispiel 134 schreibst, hast du schon mit dem Arduino, Calliope und dem B-O-B-3 gearbeitet!\nFalls du noch nichts mit diesen Geräten gemacht hast drücke ENTER", "Hardware: ")
    which_skills = Q("Bitte gebe an, was du schonmal programmiert hast (falls vorhanden).\nWenn du noch nichts programmiert hast, drücke einfach ENTER", "Erfahrungen: ")
    what_try = Q("Bitte gebe an, was du gerne bei HTTF ausprobieren willst.", "Ideen: ")
    keen_on = Q("Bitte gebe an, auf was du dich bei HTTF freust.", "Worauf du dich freust: ")
    cando = Q("Bitte gebe an, welche weiteren Fähigkeiten du bei HTTF einbringen könntest.", "Fähigkeiten: ")
    db_laptop = Q("Bitte gebe an, ob du deinen eigenen Laptop für HTTF verwendest.\nWenn du einen hast drücke ENTER, wenn du einen brauchst schreibe 'need'.", "Laptop: " )

#    if db_laptop == "":


    conn = sqlite3.connect("data.db")
    c = conn.cursor()

    c.execute("""CREATE TABLE data (
        _forename text,
        _lastname text,
        _street text,
        _house_number text,
        _plz text,
        _city text,
        _email text,
        _gender text,
        _birthday text,
        _phone_number text,
        _allergic text,
        _parent_name text,
        _phone_number_parent text,
        _email_parent text,
        _db_have_programmed text,
        _programming_language text,
        _db_hardware text,
        _which_skills text,
        _what_try text,
        _keen_on text,
        _cando text,
        _db_laptop text
    )""")

    c.execute("INSERT INTO data (_forename, _lastname, _street, _house_number, _plz, _city, _email, _gender, _birthday, _phone_number, _allergic, _parent_name, _phone_number_parent, _email_parent, _db_have_programmed, _programming_language, _db_hardware, _which_skills, _what_try, _keen_on, _cando, _db_laptop) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (forename, lastname, street, house_number, plz, city, email, gender, birthday, phone_number, allergic, parent_name, phone_number_parent, email_parent, db_have_programmed, programming_language, db_hardware, which_skills, what_try, keen_on, cando, db_laptop))

    conn.commit()

    print("\nDeine Daten Wurden Gespeichert\n")



def hashMD5(my_string):
    m = hashlib.md5()
    m.update(my_string.encode('utf-8'))
    return m.hexdigest()



def EncryptData():
    a=2
    while True:
        print("Möchtest du deine Daten versclüsseln? [y/N]")
        wantToEnc = input("[y/N]» ")

        if wantToEnc.lower().strip() == "y":

            while True:
                print("Tippe dein Passwort ein:")
                password = input("$» ")
                #Password Check
                if password.find(' ') > 0:
                    print("ERROR: \n Benutzte KEINE Leerzeichen! \n Versuch es nochmal!")
                else:
                    key = hashMD5(password)
                    #print("Key: " + key)

                    if os.path.isfile('./data.enc.zip') is True:
                        os.system('rm data.enc.zip')

                        if platform.system() is "Windows":
                           os.system("del data.enc.zip")

                    if platform.system() is "Windows":
                        os.system(r'"C:\Program Files\7-Zip\7z.exe" a -p' + key + ' data.db.enc data.db')
                        os.system('del data.db')
                    else:
                        os.system('7za a -tzip -p' + key + ' -mem=AES256 data.db.enc data.db')
                        #os.system('zip -P ' + key + ' data.enc.zip data.db #>/dev/null 2>&1')
                        os.system('rm data.db')
                    a = 3
                    break
            if a == 3:
                break

        elif wantToEnc.lower().strip() == "n":

            if platform.system() is "Windows":
                os.system("del data.enc.zip")
            else:
                os.system("rm data.enc.zip")

            print("Bye.")
            break

        else:
            print("ERROR: \n" + wantToEnc + " is Invalid  \n Versuch es nochmal!")



def heading():
        spaces = " " * 76
        sys.stdout.write(GREEN + spaces + """
         __      __ ____________ ____________ ________
        |  |    |  |_____   ____|_____   ____|   _____|
        |  |____|  |    |  |         |  |    |  |_____
        |   ____   |    |  |         |  |    |  ______|
        |  |    |  |    |  |         |  |    |  |
        |__|    |__|    |__|         |__|    |__|
                ____     _________   _________
               |    |   |   ______| |   ______|
              |  __  |  |  |        |  |
              | |__| |  |  |        |  |
             |   __   | |  |______  |  |______
             |__|  |__| |_________| |_________|

 ______ _____ _     _ _____ ______   __ ______ _____ ______
|  ____|  ___| \   | |  ___|  __  | |  |___  _|     |  __  |
| | ___| |___|   \ | | |___| |__| || __ | | | |  _  | |__| |
| ||_  |  ___|  \    |  ___|  __ | ||__|| | | | |_| |  __ |
| |__| | |___| | \   | |___| |  \ \| __  || | |     | |  \ \\
|______|_____|_|  \|_|_____|_|   \|_|  |_||_| |_____|_|   \_\\

        """ + END + BLUE +
        '\n' + 'Made by: {0}Sascha S. ({1}Mv0sKff{2}) & {0}Maris B. ({1}TheProgramming_M{2}){3}'.format(YELLOW, RED, YELLOW, BLUE).center(111) +
        '\n' + 'Version: {}2.1{} \n'.format(YELLOW, END).center(86))


if __name__ == "__main__":
    heading()
    if (opsys == "Linux"):
        LinuxInstall()
    elif (opsys == "Windows"):
        WindowsInstall()
    elif (opsys == "Darwin"):
        macInstall()

    QuestionDialogAndDoSqllite()
    EncryptData()
    heading()
    print("\nFinished! Now you can run 'python3 main.py'")
    sys.exit(0)
#    time.sleep(3)
#    if opsys is "Windows":
#        os.system("cls")
#    else:
#        os.system("clear")
